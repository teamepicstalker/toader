function love.conf(t)
	t.title             = "Toader"
	t.author            = "ToadRider Studios"
	t.version           = "0.10.1"
	t.identity          = "Toader"

	t.console           = false
	t.modules.joystick  = true
    t.modules.audio     = true
    t.modules.keyboard  = true
    t.modules.event     = true
    t.modules.image     = true
    t.modules.graphics  = true
    t.modules.timer     = true
    t.modules.mouse     = true
    t.modules.sound     = true
    t.modules.physics   = true
	t.window.vsync      = true
	t.window.resizable  = false
end
