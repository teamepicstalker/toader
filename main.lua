cTimer = require "hump.timer"
Vector2 = require "hump.vector"
cAnimation = require 'anim8.anim8'
Class = require "hump.class"
loveframes = require "loveframes"
loader = require "love-loader"
md5 = require "lua.md5"
http = require("socket.http")
ltn12 = require("ltn12")

function love.load()
	-- require all
	local files = loveframes.util.GetDirectoryContents("lua")
	for k, v in ipairs(files) do
		if v.extension == "lua" then
			require(v.requirepath)
		end
	end

	-- setup error log
	gErrorLog = not gErrorLog and love.filesystem.newFile("errors.log")
	if (gErrorLog) then
		gErrorLog:open("w") -- we open in write mode to wipe file clean on each application start
		gErrorLog:write("")
		gErrorLog:close()
		gErrorLog:open("a")
	end

	math.randomseed(os.time())

	-- Create the Game Manager class
	Game = cGame()
	Game:reinit()
end

function love.textinput(text)
	loveframes.textinput(text)
end
