cKeybindManager = Class{}
function cKeybindManager:init(_Game)
	-- list of connected joysticks
	self.joysticks = {}
	
	-- Keys that are unassignable
	self.unassignable = { 
				["escape"] = true
	}
	
	-- Default Keyboard layout
	self.alias = {
			["keyboard"] = {
							["pause"] 		= { "escape", "false"},
							["action"]		= { "e", "true" },
							["up"] 			= { "w", "true" },
							["left"] 		= { "a", "true" },
							["down"] 		= { "s", "true" },
							["right"]		= { "d", "true" },
							}
	}
	
	-- For Menu order
	self.aliasSort = { "pause", "action", "up", "left", "down", "right" }

	-- Check settings for saved keyboard layout
	local keymap = _Game.Config:GetKeys("keyboard")
	if (keymap) then
		for k,v in pairs(keymap) do
			self.alias.keyboard[k] = str_explode(v,",")
		end
	end
	
	-- available callbacks from this manager
	self.callbacks = {	
						["onJoystickAdded"] = {},
						["onJoystickRemoved"] = {},
						["onKeyPressed"] = {},
						["onKeyPressedRaw"] = {},
						["onMousePressed"] = {}
	}
	
	-- Load default Gamepad Mappings 
	love.joystick.loadGamepadMappings("gamecontrollerdb.txt")
	
	-- wrap ourselves in love
	love.joystickadded = function(...) self.onJoystickAdded(self,...) end
	love.joystickremoved = function(...) self.onJoystickRemoved(self,...) end
	love.joystickpressed = function(...) self.onJoystickPressed(self,...) end
	love.mousepressed = function(...) self.onMousePressed(self,...) end
	love.mousereleased = function(...) self.onMouseReleased(self,...) end
	love.gamepadpressed = function(...) self.onGamepadPressed(self,...) end
	love.keypressed = function(...) self.onKeyPressed(self,...) end
end 

function cKeybindManager:keyToAlias(key,typ)
	if not (self.alias[typ]) then 
		return 
	end
	key = tostring(key)
	for k,v in pairs(self.alias[typ]) do 
		if (v[1] == key) then
			return k 
		end
	end
end

function cKeybindManager:AliasToKey(alias,typ)
	if not (self.alias[typ]) then 
		return 
	end
	return self.alias[typ][alias][1],self.alias[typ][alias][2]
end  

function cKeybindManager:clearKeybind(key,typ)
	if not (self.alias[typ]) then 
		return 
	end
	key = tostring(key)
	for k,v in pairs(self.alias[typ]) do
		if (v[1] == key) then
			self.alias[typ][k][1] = "none"
		end
	end
end

function cKeybindManager:onJoystickAdded(joy)
	local ind = #self.joysticks
	for i=1, ind do 
		if (self.joysticks[i] == joy) then 
			return 
		end
	end
	ind = ind + 1
	
	self.joysticks[ind] = joy
	
	-- setup default for unrecognized controller (mine)
	local guid = joy:getGUID()
	if joy:getName() == "4 axis 16 button joystick" then
		love.joystick.setGamepadMapping(guid, "a", "button", 3)
		love.joystick.setGamepadMapping(guid, "dpup", "button", 13)
		love.joystick.setGamepadMapping(guid, "dpright", "button", 14)
		love.joystick.setGamepadMapping(guid, "dpdown", "button", 15)
		love.joystick.setGamepadMapping(guid, "dpleft", "button", 16)
		love.joystick.setGamepadMapping(guid, "start", "button", 12)
	end
	
	-- create the alias table
	self.alias[guid] = {
						["pause"] 		= { "start", "true" },
						["action"]		= { "a", "true" },
						["up"] 			= { "dpup", "true" },
						["left"] 		= { "dpleft", "true" },
						["down"] 		= { "dpdown", "true" },
						["right"]		= { "dpright", "true" },
	}
	
	-- re-assign LuaLOVE virtual gamepad alias to actual button number
	for k,v in pairs(self.alias[guid]) do 
		a,b,c = joy:getGamepadMapping(v[1])
		self.alias[guid][k][1] = tostring(b)
	end
	
	-- overwrite button keybind with data config 
	local keymap = Game.Config:GetKeys(guid)
	if (keymap) then
		for k,v in pairs(keymap) do
			self.alias[guid][k] = str_explode(v,",")
		end
	end	
	
	-- send callback
	self:callback("onJoystickAdded",joy,ind)
end

function cKeybindManager:onJoystickRemoved(joy)
	local ind
	for i=1,#self.joysticks do 
		if (self.joysticks[i] == joy) then 
			ind = i 
			break
		end
	end
	if (ind) then
		table.remove(self.joysticks,ind)
	end
	-- send callback
	self:callback("onJoystickRemoved",joy,ind)
end

function cKeybindManager:joystickIndex(joy)
	for i=1,#self.joysticks do 
		if (self.joysticks[i] == joy) then 
			return i
		end
	end
end

function cKeybindManager:register(name,func,obj)
	assert(func)
	assert(self.callbacks[name])
	local function wrapper(...)
		func(obj,...)
	end
	self.callbacks[name][obj] = wrapper
end

function cKeybindManager:unregister(name,obj)
	if (self.callbacks[name]) then
		self.callbacks[name][obj] = nil
	end
end

function cKeybindManager:registerJoystick(joy,name,func,obj)
	if not (self.callbacks[joy]) then
		self.callbacks[joy] = {}
	end
	if not (self.callbacks[joy][name]) then 
		self.callbacks[joy][name] = {}
	end
	local function wrapper(...)
		func(obj,...)
	end
	self.callbacks[joy][name][obj] = wrapper
end

function cKeybindManager:unregisterJoystick(joy,name,obj)
	if (self.callbacks[joy] and self.callbacks[joy][name]) then
		self.callbacks[joy][name][obj] = nil
	end
end

function cKeybindManager:callback(name,...)
	if not (self.callbacks[name]) then
		return 
	end
	
	for k,func in pairs(self.callbacks[name]) do
		func(...)
	end 
end

function cKeybindManager:callbackJoystick(joy,name,...)
	if not (self.callbacks[joy] and self.callbacks[joy][name]) then
		return
	end
	for k,func in pairs(self.callbacks[joy][name]) do
		func(...)
	end
end
function cKeybindManager:onGamepadPressed(joy,key)

end
function cKeybindManager:onKeyPressed(key,scancode,bRepeat)
	--[[
	if (scancode == "q") then 
		DEBUG = not DEBUG
	end
	--]]
	local alias = self:keyToAlias(key,"keyboard")
	if (alias) then
		self:callback("onKeyPressed",alias,key,scancode,bRepeat)
	end
	-- raw is without alias check
	self:callback("onKeyPressedRaw",key,scancode,bRepeat)
	loveframes.keypressed(key,bRepeat)
end
function cKeybindManager:onMousePressed(x,y,button)
	self:callback("onMousePressed",x,y,button)
	loveframes.mousepressed(x, y, button)
end 

function cKeybindManager:onMouseReleased(x,y,button)
	loveframes.mousereleased(x,y,button)
end

function cKeybindManager:onJoystickPressed(joystick,key)
	key = tostring(key)
	local ind = self:joystickIndex(joystick)
	local alias = self:keyToAlias(key,joystick:getGUID())
	if (alias) then	
		self:callbackJoystick(ind,"onGamepadPressed",alias,key)
	end
	-- raw is without alias check
	self:callbackJoystick(ind,"onGamepadPressedRaw",key,joystick,ind)
end