cSoundManager = Class{}
function cSoundManager:init()
	self.sfx = {}
	self.music = {}

	-- Register all sounds
	self:addSFX("mouseover","assets/sounds/mouseover.ogg")
	self:addSFX("crush","assets/sounds/crush.ogg")
	self:addSFX("gameover","assets/sounds/gameover.ogg")
	self:addSFX("splash","assets/sounds/splash.ogg")
	self:addSFX("hop","assets/sounds/hop.ogg")

	self:addMusic("music","assets/sounds/music.ogg")
	--self:playMusic("music")
	
	RegisterScriptCallback("onLoaderFinished",self.onLoaderFinished,self)
end

function cSoundManager:onLoaderFinished()
	for alias,sound in pairs(self.sfx) do
		sound:setVolume(self:getVolumeSFX()*self:getVolumeMaster())
	end
	for alias,sound in pairs(self.music) do
		sound:setVolume(self:getVolumeMusic()*self:getVolumeMaster())
		sound:setLooping(true)
	end
	
	self:playMusic("music")
end

function cSoundManager:setVolumeMaster(n)
	Game.Config:SetValue("sound","master_volume",n)
	self:setVolumeSFX(self:getVolumeSFX())
	self:setVolumeMusic(self:getVolumeMusic())
end

function cSoundManager:getVolumeMaster()
	return Game.Config:r_number("sound","master_volume") or 1
end

function cSoundManager:setVolumeSFX(n)
	for k,v in pairs(self.sfx) do
		v:setVolume(n*self:getVolumeMaster())
	end
	Game.Config:SetValue("sound","sfx_volume",n)
end

function cSoundManager:setVolumeMusic(n)
	for k,v in pairs(self.music) do
		v:setVolume(n*self:getVolumeMaster())
	end
	Game.Config:SetValue("sound","music_volume",n)
end

function cSoundManager:getVolumeSFX()
	return Game.Config:r_number("sound","sfx_volume") or 1
end

function cSoundManager:getVolumeMusic()
	return Game.Config:r_number("sound","music_volume") or 1
end

function cSoundManager:addSFX(alias,path)
	--self.sfx[alias] = love.audio.newSource(path,"static")
	loader.newSource(self.sfx,alias,path)
	--assert(self.sfx[alias])
	--self.sfx[alias]:setVolume(self:getVolumeSFX()*self:getVolumeMaster())
end

function cSoundManager:addMusic(alias,path)
	--self.music[alias] = love.audio.newSource(path)
	loader.newSource(self.music,alias,path,"stream")
	--assert(self.music[alias])
	--self.music[alias]:setLooping(true)
	--self.music[alias]:setVolume(self:getVolumeMusic()*self:getVolumeMaster())
end

function cSoundManager:playSFX(alias)
	assert(self.sfx[alias])
	self.sfx[alias]:stop()
	self.sfx[alias]:play()
end

function cSoundManager:playMusic(alias)
	assert(self.music[alias])
	self.music[alias]:stop()
	self.music[alias]:play()
end
