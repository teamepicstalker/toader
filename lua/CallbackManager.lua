cCallbackManager = Class{}
function cCallbackManager:init()
	self.callbacks = {}
	RegisterScriptCallback = function (...) self.register(self,...) end
	UnregisterScriptCallback = function (...) self.unregister(self,...) end
	SendScriptCallback = function (...) self.callback(self,...) end
end
function cCallbackManager:register(name,func,obj)
	assert(func)
	if not (self.callbacks[name]) then
		self.callbacks[name] = {}
	end
	local function wrapper(...)
		func(obj,...)
	end
	self.callbacks[name][obj] = wrapper
end
function cCallbackManager:unregister(name,obj)
	if (self.callbacks[name]) then
		self.callbacks[name][obj] = nil
	end
end
function cCallbackManager:callback(name,...)
	if not (self.callbacks[name]) then
		return
	end

	for k,func in pairs(self.callbacks[name]) do
		func(...)
	end
end
