------------------------------------------------
-- helpers
------------------------------------------------
function printf(msg,...)
	msg = tostring(msg)
	if (select('#',...) >= 1) then
		local i = 0
		local p = {...}
		local function sr(a)
			i = i + 1
			if (type(p[i]) == 'userdata') then
				return 'userdata'
			end
			return tostring(p[i])
		end
		msg = string.gsub(msg,"%%s",sr)
	end
	print(msg)
end

function directory_exists(path)
	return os.execute( "CD " .. path ) == 0
end

function file_exists(path)
	return io.open(path) ~= nil
end

function get_ext(s)
	return string.gsub(s,"(.*)%.","")
end

function startsWith(text,prefix)
	return string.sub(text, 1, string.len(prefix)) == prefix
end

function trim(s)
	return string.gsub(s, "^%s*(.-)%s*$", "%1")
end

function get_path(str,sep)
    sep=sep or'\\'
    return str:match("(.*"..sep..")")
end

function str_explode(str,div,dont_trim)
	if not (dont_trim) then
		trim(str)
	end
	local t={}
	local cpt = string.find (str, div, 1, true)
	local a
	if cpt then
		repeat
			if not dont_trim then
				a = trim(string.sub(str, 1, cpt-1))
				table.insert( t, a )
			else
				table.insert( t, string.sub(str, 1, cpt-1) )
			end
			str = string.sub( str, cpt+string.len(div) )
			cpt = string.find (str, div, 1, true)
		until cpt==nil
	end
	if not dont_trim then
		a = trim(str)
		table.insert(t, a)
	else
		table.insert(t, str)
	end
	return t
end

function literal(str)
    return str:gsub("[%(%)%.%%%+%-%*%?%[%]%^%$]", "%%%0")
end

-- INI Reader
function file_to_table(file)
	local root = {}
	local sec_includes
	local sec, t,_root,key,val
	for line in file:lines() do
		if (line ~= "" and line ~= "\n") then
			if (startsWith(line, "[")) then
				t = str_explode(line,";")
				t = str_explode(t[1],":")

				sec = string.sub(t[1],2,-2)
				root[sec] = {}
			elseif (not startsWith(line, ";") and not startsWith(line,"	") and not startsWith(line," ")) then
				t = str_explode(line,";")
				key = trim(string.match(t[1],"(.-)=") or t[1])
				if (key and key ~= "") then
					key = trim(key)
					val = string.match(t[1],"=(.+)")
					if (val) then
						val = trim(val)
					end

					if (sec) then
						root[sec] = root[sec] or {}
						root[sec][key] = val or ""
					else
						root[key] = val or ""
					end
				end
			end
		end
	end

	return root
end



function rem_quotes(txt)
	if not (txt) then return end
	for w in string.gmatch(txt,"[\"'](.+)[\"']") do
		return w
	end
	return txt
end

function recurse_subdirectories_and_execute(node,ext,func)
	local stack = {}
	local deepest
	while not deepest do
		if (node) then
			for file in lfs.dir(node) do
				if lfs.attributes(file,"mode") == "file" then
					printf(file)
				elseif lfs.attributes(file,"mode") == "directory" then
					if (file ~= "..") then
						for l in lfs.dir(node.."\\"..file) do
							if (l ~= ".." and l ~= ".") then
								if lfs.attributes(node.."\\"..file.."\\"..l,"mode") == "file" then
									for i=1,#ext do
										if (get_ext(l) == ext[i]) then
											func(node,l)
										end
									end
								elseif lfs.attributes(node.."\\"..file.."\\"..l,"mode") == "directory" then
									--print(node .. "\\"..l)
									table.insert(stack,node .. "\\" .. l)
								end
							end
						end
					end
				end
			end
		end

		if (#stack > 0) then
			node = stack[#stack]
			stack[#stack] = nil
		else
			deepest = true
		end
	end
end

function copy_file_data(file,fullpath,data,overwrite)
	if not (file) then
		return 0
	end

	if (not file_exists(fullpath) or overwrite) then
		-- create the directory for a newly copied file
		for dir in string.gmatch(fullpath,"(.+)\\","") do
			dir = trim(dir)
			if not (directory_exists(dir)) then
				os.execute('MD "'..dir..'"')
			end
		end
		local output_file = io.open(fullpath,"wb+")
		if (output_file) then
			data = data or file:read("*all")
			if (data) then
				output_file:write(data)
				output_file:close()
				return 1
			end
		else
			return -1
		end
	else
		return 2
	end
	return 0
end

function math.round(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

function math.clamp(x, min, max)
  return x < min and min or (x > max and max or x)
end

function table.copy(a)
	local t = {}
	for k,v in pairs(a) do
		t[k] = v
	end
	return t
end


function log(msg,...)
	if not (gErrorLog) then
		return
	end
	msg = tostring(msg)
	if (select('#',...) >= 1) then
		local i = 0
		local p = {...}
		local function sr(a)
			i = i + 1
			if (type(p[i]) == 'userdata') then
				return 'userdata'
			end
			return tostring(p[i])
		end
		msg = string.gsub(msg,"%%s",sr)
	end
	gErrorLog:write(msg.."\n")
end

function strformat(text,...)
	if not (text) then return end
	local i = 0
	local p = {...}
	local function sr(a)
		i = i + 1
		if (type(p[1]) == "userdata") then
			return "userdata"
		end
		return tostring(p[i])
	end
	return string.gsub(text,"%%s",sr)
end

local function _formencodepart(s)
	return s and (s:gsub("%W", function (c)
		if c ~= " " then
			return string.format("%%%02x", c:byte());
		else
			return "+";
		end
	end));
end

function formencode(form)
	local result = {};
	if form[1] then -- Array of ordered { name, value }
		for _, field in ipairs(form) do
			table.insert(result, _formencodepart(field.name).."=".._formencodepart(field.value));
		end
	else -- Unordered map of name -> value
		for name, value in pairs(form) do
			table.insert(result, _formencodepart(name).."=".._formencodepart(value));
		end
	end
	return table.concat(result, "&");
end
