cSpriteManager = Class{}
function cSpriteManager:init(id,pos,angle,anim,no_clone)
	self.id = id
	self.position = pos 
	self.angle = r
	self.layer = nil
	self.layerForced = nil
	self.visible = true
	if (anim) then
		self:setState(anim,no_clone)
	end
end

function cSpriteManager:update(dt)
	if not (self.visible) then 
		return 
	end
	
	--log("update %s %s",self.state,self.animation)
	if not (self.state or self.animation) then 
		return 
	end
	
	if (self.state == "water") then
		--log("player x=%s y=%s  Tile x=%s y=%s",oPlayer.grid_position.x,oPlayer.grid_position.y,self.position.x,self.position.y)
		if (oPlayer.isDead ~= true and oPlayer.isMoving ~= true and oPlayer.holderID == nil and oPlayer:checkCollision(self.position.x,self.position.y,62,62)) then
			oPlayer:makeDead()
			oPlayer.sprite:setState("drown",true)
			oSoundManager:playSFX("splash")
		end
	end
	
	self.animation:update(dt)
end 

function cSpriteManager:setLayer(layer)
	if (self.layer == layer) then 
		return 
	end
	
	-- remove sprite from old layer
	if (self.layer ~= nil and CurrentLevel:map().layers[self.layer].sprites) then 
		CurrentLevel:map().layers[self.layer].sprites[self.id] = nil
	end
	
	-- point to new layer
	self.layer = layer
	
	-- return because we don't have a current layer
	if (layer == nil) then 
		return 
	end
	
	-- set new sprite layer
	if not (CurrentLevel:map().layers[layer].sprites) then 
		CurrentLevel:map().layers[layer].sprites = {}
	end
	CurrentLevel:map().layers[layer].sprites[self.id] = self
end

function cSpriteManager:draw()
	if not (self.visible) then 
		return 
	end

	--log("draw %s %s",self.state,self.animation)
	if not (self.state or self.animation) then 
		return 
	end
	
	local a = AnimationLib[self.state]
	self.animation:draw(a.image,self.position.x,self.position.y,self.angle,a.scale.x,a.scale.y,a.offset.x,a.offset.y,a.shear.x,a.shear.y)
	
	local obj = CurrentLevel:objectByID(self.id)
	if (obj and obj.onDraw) then
		obj:onDraw()
	end
end

function cSpriteManager:getWidth()
	return AnimationLib[self.state] and AnimationLib[self.state].width or 0
end 
function cSpriteManager:getHeight()
	return AnimationLib[self.state] and AnimationLib[self.state].height or 0
end 

function cSpriteManager:setState(anim,no_clone,reset)
	if (anim == nil) then 
		self:setLayer(nil)
		self.visible = false
		return
	end

	if not (AnimationLib[anim] and AnimationLib[anim].animation) then 
		log("animation doesn't exist %s",anim)
		return
	end

	if (self.state == anim) then 
		return 
	end
	
	-- reset previous animation
	if (self.animation) then 
		self.animation.position = 1
		self.animation.timer = 0
		self.animation:resume()
	end
	
	-- Here we switch layers if self.layer has changed
	if (AnimationLib[anim].layer ~= "Dynamic") then
		self:setLayer(self.layerForced or AnimationLib[anim].layer)
	end
	
	-- set new animation
	self.state = anim
	self.animation = no_clone and AnimationLib[anim].animation or AnimationLib[anim].animation:clone()
	self.animation:setCallback(self.callback,self)
	
	self.animation.position = 1
	self.animation.timer = 0
	self.animation:resume()
	
	local obj = CurrentLevel:objectByID(self.id)
	if (obj) then
		obj:resetBoundingBox()
	end
end

function cSpriteManager:callback(animation,count)
	if (self.animation ~= animation) then 
		return 
	end
	if (AnimationLib[self.state].looped ~= true) then
		animation.position = #animation.frames
		animation.timer = animation.totalDuration
		animation:pause()
	end
	local obj = CurrentLevel:objectByID(self.id)
	if (obj and obj.animation_callback) then 
		obj:animation_callback(self,animation,count)
	end
end

function cSpriteManager:animationAtEnd()
	if not (self.animation) then 
		return true
	end
	return self.animation.position == #self.animation.frames and self.animation.timer >= self.animation.totalDuration
end

function cSpriteManager:getState()
	return self.state
end

function cSpriteManager:getAnimation()
	return self.animation
end

function cSpriteManager:destroy()
	if (self.layer ~= nil and CurrentLevel:map().layers[self.layer].sprites) then 
		CurrentLevel:map().layers[self.layer].sprites[self.id] = nil
	end
end