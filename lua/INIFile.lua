cIniFile = Class{}
function cIniFile:init(fname)
	cfg = love.filesystem.newFile(fname)
	if not (cfg) then
		return
	end

	cfg:open("a")
	cfg:close()

	self.file = cfg
	self.fname = fname
	self.root = file_to_table(cfg)
	return self
end

function cIniFile:r_bool(sec,key)
	return self.root and self.root[sec] and self.root[sec][key] == "true" or false
end
function cIniFile:r_number(sec,key)
	return self.root and self.root[sec] and tonumber(self.root[sec][key])
end
function cIniFile:r_string(sec,key)
	return self.root and self.root[sec] and tostring(self.root[sec][key])
end

function cIniFile:GetKeys(sec)
	return self.root and self.root[sec]
end

function cIniFile:SetValue(sec,key,val)
	if not (self.root) then
		self.root = {}
	end

	if not (self.root[sec]) then
		self.root[sec] = {}
	end

	self.root[sec][key] = val == nil and "" or tostring(val)
end

function cIniFile:SectionExist(sec)
	return self.root and self.root[sec] ~= nil
end

function cIniFile:KeyExist(sec,key)
	return self.root and self.root[sec] and self.root[sec][key] ~= nil
end

-- Save ini by preserving original file. Cannot insert new keys or sections
function cIniFile:SaveExt()
	local t,sec,comment
	local str = ""

	local function addTab(s,n)
		local l = string.len(s)
		for i=1,n-l do
			s = s .. " "
		end
		return s
	end

	for ln in self.file:lines() do
		ln = trim(ln)
		if (startsWith(ln,"[")) then
			t = str_explode(ln,";")
			t = str_explode(t[1],":")

			sec = string.sub(t[1],2,-2)
		elseif (not startsWith(ln,";") and self.root[sec]) then
			comment = string.find(ln,";")
			comment = comment and string.sub(ln,comment) or ""

			if (comment ~= "") then
				comment = addTab("\t",40) .. comment
			end

			t = str_explode(ln,"=")
			if (self.root[sec][t[1]] ~= nil) then
				if (self.root[sec][t[1]] == "") then
					ln = addTab(t[1],40) .. " =" .. comment

				else
					ln = addTab(t[1],40) .. " = " .. tostring(self.root[sec][t[1]]) .. comment
				end
			end
		end
		str = str .. ln .. "\n"
	end
	self.file:open("w")
	self.file:write(str)
	self.file:close()
end

-- Recreates ini as stored in the table
function cIniFile:Save()
	local str = "",count

	for section,tbl in pairs(self.root) do
		str = str .. "[" .. section .. "]\n"

		if (self.links and self.links[section]) then
			str = str .. ":"
			count = #self.links[section]
			for i=1,count do
				if (count > 1 and i ~= count) then
					str = str .. self.links[section][i] .. ","
				else
					str = str .. self.links[section][i]
				end
			end
		end

		for key,val in pairs(self.root[section]) do
			if (val == "") then
				str = str .. key .. "\n"
			else
				str = str .. key .. " = " .. tostring(val) .. "\n"
			end
		end
	end

	self.file:open("w")
	self.file:write(str)
	self.file:close()
end
