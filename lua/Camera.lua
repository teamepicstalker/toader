local cos, sin = math.cos, math.sin

cCamera = Class{}
function cCamera:init(pos)
	self.target_id = nil
	self.position = pos or Vector2(0,0)
	RegisterScriptCallback("onUpdate",self.onUpdate,self)
	self.scale = Game.Config:r_number("graphics","scale") or 1
	self.width = windowWidth/(2*self.scale)
	self.height = windowHeight/(2*self.scale)
	self.offset = (windowWidth-mapWidth)/(2*self.scale)
	self.rot = 0
end 
function cCamera:onUpdate(dt)
	if (self.target_id) then
		local go = CurrentLevel:objectByID(self.target_id)
		if (go) then
			self.position.x = self.position.x - (self.position.x - go.position.x) * 6 * dt
			self.position.y = self.position.y - (self.position.y - go.position.y) * 6 * dt
			
			self.position.x = math.clamp(self.position.x,0+self.width-self.offset,mapWidth-self.width+self.offset)
			self.position.y = math.clamp(self.position.y,0+self.height,mapHeight-self.height)
		end
	end
end 
function cCamera:setTarget(id)
	self.target_id = id
end
function cCamera:attach()
	love.graphics.push()
	love.graphics.scale(self.scale)
	love.graphics.translate(self.width,self.height)
	love.graphics.rotate(self.rot)
	love.graphics.translate(-self.position.x, -self.position.y)
end
function cCamera:detach()
	love.graphics.pop()
end

function cCamera:rotate(phi)
	self.rot = self.rot + phi
	return self
end

function cCamera:rotateTo(phi)
	self.rot = phi
	return self
end

function cCamera:cameraCoords(x,y)
	-- x,y = ((x,y) - (self.x, self.y)):rotated(self.rot) * self.scale + center
	local w,h = love.graphics.getWidth(), love.graphics.getHeight()
	local c,s = cos(self.rot), sin(self.rot)
	x,y = x - self.x, y - self.y
	x,y = c*x - s*y, s*x + c*y
	return x*self.scale + w/2, y*self.scale + h/2
end

function cCamera:worldCoords(x,y)
	-- x,y = (((x,y) - center) / self.scale):rotated(-self.rot) + (self.x,self.y)
	local w,h = love.graphics.getWidth(), love.graphics.getHeight()
	local c,s = cos(-self.rot), sin(-self.rot)
	x,y = (x - w/2) / self.scale, (y - h/2) / self.scale
	x,y = c*x - s*y, s*x + c*y
	return x+self.x, y+self.y
end

function cCamera:mousePosition()
	return self:worldCoords(love.mouse.getPosition())
end

function cCamera:boundary()
	return 0,self.position.y-576,mapWidth,1152
end

function cCamera:resetViewport()
	self.width = windowWidth/(2*self.scale)
	self.height = windowHeight/(2*self.scale)
	self.offset = (windowWidth-mapWidth)/(2*self.scale)
end