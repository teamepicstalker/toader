cSpawnEvent = Class{}
function cSpawnEvent:init(pos,tmr)
	self.position = pos
	self.event = tmr
end 

function cSpawnEvent:update(dt)
	if (math.abs(oCamera.position.x-self.position.x) <= windowWidth+128) then 
		if (math.abs(oCamera.position.y-self.position.y) <= windowHeight) then
			self.event.update(dt)
		end 
	end
end

--[[
Spawn Manager interprets custom properties of map.lua created with Tiled
x - Override x position instead of using the tile position x-value
position - Is calculated by custom property 'x' and/or tile position rounded to value divisible by 64 (tile width)
class - which class the object is 
rate - time in seconds to re-spawn. Rate 0 or below will disable respawning
speed - the velocity the object moves that is passed to the class init method
sprite - the custom sprite state used, can be a list separated by commas
start - Intial spawn positions for the object, only x-value supported. Can be a list separated by commas 
path - x,y offsets for the object to move towards. A list of path points is separated by ';'
--]]
cSpawnManager = Class{}
function cSpawnManager:init()
	self.events = {}
	CurrentLevel:map().layers.spawn.visible = false
	for k,v in pairs(CurrentLevel:map().layers.spawn.objects) do
		local x = tonumber(v.properties.x) or math.round(v.x/64)*64
		
		-- this need scoped inside the for-loop because they are referenced later by tmr function; otherwise they would be overwritten
		local position = Vector2(x, math.round(v.y/64-1)*64 )
		
		v.properties.rate = tonumber(v.properties.rate) or 0
		v.properties.speed = tonumber(v.properties.speed)
		v.properties.sprite = str_explode(v.properties.sprite,",")
		v.properties.start = v.properties.start and v.properties.start ~= "" and str_explode(v.properties.start,",") or {}
		
		table.insert(v.properties.start,x)
		
		v.properties.path = v.properties.path and v.properties.path ~= "" and str_explode(v.properties.path,";") or {}
		
		for i=1,#v.properties.path do 
			local a = str_explode(v.properties.path[i],",")
			v.properties.path[i] = Vector2(a[1] and tonumber(a[1]) or 0,a[2] and tonumber(a[2]) or 0)
		end

		if (v.properties.rate > 0) then 
			local tmr = cTimer.new()
			tmr.every(v.properties.rate,function()
				-- pass params to spawn managers createObject based on SpawnList table
				local obj = self:createObject(v.properties.class,position:clone(),v.properties)
				assert(obj,"Failed to create object "..v.properties.class)
			end)
			table.insert(self.events,cSpawnEvent(position,tmr))
		end
		
		if (v.properties.start) then
			for n=1,#v.properties.start do
				x = tonumber(v.properties.start[n])
				if (x) then
					local pos = position:clone()
					pos.x = x
					local tmr = cTimer.new()
					tmr.after(n*.1,function()
						local obj = self:createObject(v.properties.class,pos,v.properties)
						assert(obj,"Failed to create object "..v.properties.class)
					end)
					table.insert(self.events,cSpawnEvent(position,tmr))
				end				
			end
		end
	end
end

function cSpawnManager:update(dt)
	for i=1,#self.events do 
		self.events[i]:update(dt)
	end
end

function cSpawnManager:createObject(class,...)
	assert(_G[class])
	
	local obj
	
	-- here we recycle objects instead of creating garbage
	-- objects will be re-inited and re-register their retired game object ID
	local t = CurrentLevel.__recycle[class]
	if (t and #t > 0) then
		obj = t[#t]
		t[#t] = nil
		obj:init(...)
	end
	
	-- here we create a new object if nothing recycled
	if not (obj) then
		obj = _G[class](...)
		obj.class = class
	end
	
	return obj
end