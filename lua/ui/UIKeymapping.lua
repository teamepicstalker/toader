cUIKeymapping = Class{__includes={cUIScriptWnd}}
function cUIKeymapping:init(owner,name,msg,onYes,onNo)
	cUIScriptWnd.init(self,owner,name)
end

function cUIKeymapping:initFrame()
	-- Create Frame for Sound Menu
	self.frame:SetName("Keymapping")
	self.frame:SetSize(500, 370)
	self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
	self.frame.OnClose = function(object)
		self:onQuit()
	end

	-- Create Tabs
	self:createTabs()

	-- Back Button
	local btn = loveframes.Create("button", self.frame)
	btn:SetFont(loveframes.fonts["punkboy18"])
	btn:SetPos(275,330)
	btn:SetText("Back")
	btn:SetSize(60,30)
	btn.OnClick = function(object,x,y)
		self:onQuit()
	end
	btn.OnMouseEnter = onmouse
	table.insert(self.element,btn)
end

function cUIKeymapping:createTabs()
	-- Create new tab object
	local tabs = loveframes.Create("tabs", self.frame)
	self.frame.tabs = tabs
	tabs:SetPos(5, 30)
	tabs:SetSize(490, 335)

	-- Keyboard Tab
	local panel = loveframes.Create("panel")
	panel.Draw = function()	end
	self:createKeymapList("keyboard",panel)
	tabs:AddTab("Keyboard",panel,"Keyboard")

	-- Joystick Tabs
	for i=1, #oKeybindManager.joysticks do
		local panel = loveframes.Create("panel")
		self:createKeymapList("joystick",panel,i)
		panel.Draw = function() end
		local str = oKeybindManager.joysticks[i]:getName()
		tabs:AddTab(str, panel, str)
	end
end

function cUIKeymapping:createKeymapList(name,tab,idx)
	-- grab the joystick guid
	local guid = name == "joystick" and oKeybindManager.joysticks[idx]:getGUID() or "keyboard"

	-- Setup default OnMouseEnter
	local onmouse = function(object)
		oSoundManager:playSFX("mouseover")
	end

	local txt = {}
	txt[1] = { color = {255,255,255,255}, font = loveframes.fonts[18] }

	-- Set Captions
	txt[2] = "Alias"
	local text = loveframes.Create("text", tab)
	text:SetPos(45,5)
	text:SetText(txt)

	txt[2] = "Key"
	local text = loveframes.Create("text", tab)
	text:SetPos(160,5)
	text:SetText(txt)

	-- Create a List to display Alias and Keybind side-by-side
	local list = loveframes.Create("list", tab)
	list:SetPos(0, 30)
	list:SetSize(240, 270)
	list:SetPadding(5)
	list:SetSpacing(5)
	list:EnableHorizontalStacking(true)

	-- local elements scoped to this tab
	local element = {}

	-- get a list of active aliases
	local aliases = oKeybindManager.aliasSort

	-- Update all text fields for the buttons
	local function resetAll()
		for i=1,#element do
			local a = oKeybindManager.alias[guid] and oKeybindManager.alias[guid][ aliases[i] ][1]
			element[i]:SetText(a or "none")
		end
	end

	for i=1, #aliases do
		local button = loveframes.Create("button")
		button:SetSize(110,30)
		button:SetText(aliases[i])
		button:SetClickable(false)
		button.CheckHover = function() end
		button:SetFont(loveframes.fonts["punkboy18"])
		list:AddItem(button)

		button = loveframes.Create("button")
		button:SetSize(110,30)

		local a = oKeybindManager.alias[guid] and oKeybindManager.alias[guid][aliases[i]][1]
		local unassignable = oKeybindManager.alias[guid] and oKeybindManager.alias[guid][aliases[i]][2] == "false"

		button:SetText(a or "none")

		button:SetFont(loveframes.fonts[18])
		button.OnMouseEnter = onmouse
		if (unassignable) then
			-- don't allow assigning off unassignable keys
			button.CheckHover = function() end
			button:SetClickable(false)
		else
			button.OnClick = function(object,x,y)
				if (self.ignore_input) then
					return
				end
				self.ignore_input = true

				local panel = loveframes.Create("panel",tab)
				panel:SetPos(250,30)
				text = loveframes.Create("text", panel)
				txt[2] = "Assign next keypress"
				text:SetText(txt)
				text:SetPos(100,25,true)

				if (name == "keyboard") then
					local function tryKeybind(object,key,scancode,bRepeat)
						if not (oKeybindManager.unassignable[key]) then
							oKeybindManager:clearKeybind(key,guid)
							if (oKeybindManager.alias[guid]) then
								oKeybindManager.alias[guid][aliases[i]][1] = key
							end
							object:SetText(key)
							oKeybindManager:unregister("onKeyPressedRaw",object)
							panel:Remove()
							panel = nil
							self.ignore_input = false
							resetAll()
							-- store keybinds
							for k,v in pairs(oKeybindManager.alias[guid]) do
								Game.Config:SetValue(guid,k,table.concat(v,","))
							end
							Game.Config:Save()
						end
					end
					oKeybindManager:register("onKeyPressedRaw",tryKeybind,object)
				else
					local function tryKeybind(object,key,joystick,ind)
						oKeybindManager:clearKeybind(key,guid)
						if (oKeybindManager.alias[guid]) then
							oKeybindManager.alias[guid][aliases[i]][1] = tostring(key)
						end
						object:SetText(key)
						oKeybindManager:unregisterJoystick(idx,"onGamepadPressedRaw",object)
						panel:Remove()
						panel = nil
						self.ignore_input = false
						resetAll()
						-- store keybinds
						for k,v in pairs(oKeybindManager.alias[guid]) do
							Game.Config:SetValue(guid,k,table.concat(v,","))
						end
						Game.Config:Save()
					end
					oKeybindManager:registerJoystick(idx,"onGamepadPressedRaw",tryKeybind,object)
				end
			end
		end
		list:AddItem(button)
		--table.insert(self.element,button)
		table.insert(element,button)
	end
end

function cUIKeymapping:show(b)
	if (b == true) then
		if not (self.loaded) then
			self:initFrame()
			oKeybindManager:register("onJoystickAdded",self.resetTabs,self)
			oKeybindManager:register("onJoystickRemoved",self.resetTabs,self)
			self.loaded = true
		end
		-- We must re-create Tab objects to properly update changes such as newly detected joysticks
		self:resetTabs()
	end
	cUIScriptWnd.show(self,b)
end

function cUIKeymapping:resetTabs()
	self.frame.tabs:Remove()
	self:createTabs()
	-- move back button to top draw order
	for i=1, #self.element do
		self.element[i]:MoveToTop()
	end
end
