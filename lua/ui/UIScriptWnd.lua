cUIScriptWnd = Class{}
function cUIScriptWnd:init(owner,name)
	if (owner) then
		table.insert(owner.children,self)
	end
	self.element = {}
	self.children = {}
	self.owner = owner
	self.state = name
	self.frame = loveframes.Create("frame")
	self.frame:SetState(name)
	self.frame:ShowCloseButton(false) -- needed due to bug
	self.frame:SetDraggable(true)
	self.frame:SetScreenLocked(true)
	self.navigateIndex = 0
	self.ignore_input = false
	
	-- Register for Input callbacks
	oKeybindManager:register("onKeyPressed",self.onKeyPressed,self)
	oKeybindManager:registerJoystick(1,"onGamepadPressed",self.onKeyPressed,self)
	oKeybindManager:register("onMousePressed",self.onMousePressed,self)
end 

function cUIScriptWnd:show(b)
	if (b == true) then
		if (self.owner) then
			-- can only activate if in parent's state
			if (loveframes.GetState() == self.owner.state) then 
				-- set the state
				loveframes.SetState(self.state)
				self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
			end
		elseif (loveframes.GetState() == "none" and loveframes.GetState() ~= self.state) then
			-- activate if there are not active menu states
			loveframes.SetState(self.state)
			self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
		end
	else
		if (loveframes.GetState() == self.state) then
			-- leave state
			loveframes.SetState(self.owner and self.owner.state or "none")
		end
	end
end 

function cUIScriptWnd:isShown(check_children)
	local state = loveframes.GetState()
	if (state == self.state) then 
		return true 
	end
	if (check_children) then 
		for i=1, #self.children do 
			if (self.children[i].state == state) then 
				return true
			end
		end
	end
	return false
end

function cUIScriptWnd:onQuit()
	for i=1,#self.children do
		self.children[i]:show(false)
	end
	self:show(false)
	if (self.owner) then 
		self.owner:show(true)
	end
end

function cUIScriptWnd:finalize()
	-- unregister for Input callbacks
	oKeybindManager:unregisterJoystick(1,"OnGamepadPressed",self)
	oKeybindManager:unregister("onKeyPressed",self)
	oKeybindManager:unregister("onMousePressed",self)
	-- finalize children
	for i=1,#self.children do
		self.children[i]:show(false)
		self.children[i]:finalize()
	end
	self:show(false)
	self.frame:Remove()
	self = nil
end

function cUIScriptWnd:onKeyPressed(alias,key,code,bRepeat)
	if not (self:isShown()) then 
		return 
	end
	if (self.ignore_input) then 
		return 
	end
	if (alias == "action") then 
		self:activateElement()
	elseif (alias == "pause") then 
		self:show(not self:isShown())
	elseif (alias == "left" or alias == "right" or alias == "up" or alias == "down") then
		self:navigateWindow(alias)
	end
end 

function cUIScriptWnd:onMousePressed(x,y,button)
	if not (self:isShown()) then 
		return 
	end
	self.navigateIndex = 0
	self:navigateWindow() -- clear all selected
end

function cUIScriptWnd:navigateWindow(alias)
	local size = #self.element
	if (size == 0) then 
		return 
	end 
	
	for i=1,size do
		if (self.element[i].type == "slider") then
			if (self.element[i].internals) then 
				self.element[i].internals[1].SelectedByInput = false
			end
		else
			self.element[i].SelectedByInput = false
		end
	end
	
	if (alias == nil) then 
		return 
	end 
	
	local up = alias == "up"
	local down = alias == "down"
	local left = alias == "left"
	local right = alias == "right"

	if (self.navigateIndex == 0) then 
		if (up) then 
			self.navigateIndex = size
		else 
			self.navigateIndex = 1
		end
	else
		if (down) then
			self.navigateIndex = self.navigateIndex+1 <= size and self.navigateIndex+1 or 1
		elseif (up) then 
			self.navigateIndex = self.navigateIndex-1 >= 1 and self.navigateIndex-1 or size
		elseif (left) then
			if (self.element[self.navigateIndex].type == "slider") then
				local n = self.element[self.navigateIndex]:GetValue()
				if (n-1 >= 0) then
					self.element[self.navigateIndex]:SetValue(n-1)
				end
			elseif (self.navigateIndex-1 >= 1) then
				self.navigateIndex = self.navigateIndex-1
			end
		elseif (right) then 
			if (self.element[self.navigateIndex].type == "slider") then 
				local n = self.element[self.navigateIndex]:GetValue()
				if (n+1 <= 100) then
					self.element[self.navigateIndex]:SetValue(n+1)
				end			
			elseif (self.navigateIndex+1 <= size) then
				self.navigateIndex = self.navigateIndex+1
			end
		end
	end
	
	if (self.element[self.navigateIndex].type == "slider") then
		if (self.element[self.navigateIndex].internals) then 
			self.element[self.navigateIndex].internals[1].SelectedByInput = true
		end
	else
		self.element[self.navigateIndex].SelectedByInput = true
	end
end

function cUIScriptWnd:activateElement()
	if not (self.element[self.navigateIndex]) then 
		return 
	end
	
	if (self.element[self.navigateIndex].type == "slider") then 
		return
	else 
		self.element[self.navigateIndex].SelectedByInput = false
	end
	
	self.element[self.navigateIndex]:OnClick()
	
	self.navigateIndex = 0
end