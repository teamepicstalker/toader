cUISoundMenu = Class{__includes={cUIScriptWnd}}
function cUISoundMenu:init(...)
	cUIScriptWnd.init(self,...)
end

function cUISoundMenu:initFrame()
	-- Setup default OnMouseEnter
	local onmouse = function(object)
		oSoundManager:playSFX("mouseover")
	end

	local w,h = 300, 275
	-- Create Frame for Sound Menu
	self.frame:SetName("Sound Options")
	self.frame:SetSize(w,h)
	self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
	self.frame.OnClose = function(object)
		self:onQuit()
	end

	local txt = {}
	txt[1] = { color = {240, 240, 240,255}, font = loveframes.fonts[14] }

	for i=1,3 do
		self.element[i] = loveframes.Create("slider", self.frame)
		self.element[i]:SetPos(5,(i*45)+15)
		self.element[i]:SetWidth(250)
		self.element[i]:SetMinMax(0,100)
		self.element[i]:SetDecimals(0)
		self.element[i].internals[1].OnMouseEnter = onmouse

		if (i == 1) then
			local text = loveframes.Create("text",self.frame)
			text:SetAlwaysUpdate(true)
			text.Update = function(object,dt)
				txt[2] = "Master Volume: "..self.element[i]:GetValue().."%"
				object:SetText(txt)
			end
			text:SetShadow(true)
			text:SetPos(8,(i*45))
			self.element[i]:SetValue(oSoundManager:getVolumeMaster()*100)
		elseif (i == 2) then
			local text = loveframes.Create("text",self.frame)
			text:SetAlwaysUpdate(true)
			text.Update = function(object,dt)
				txt[2] = "Music Volume: "..self.element[i]:GetValue().."%"
				object:SetText(txt)
			end
			text:SetShadow(true)
			text:SetPos(8,(i*45))
			self.element[i]:SetValue(oSoundManager:getVolumeMusic()*100)
		else
			local text = loveframes.Create("text",self.frame)
			text:SetAlwaysUpdate(true)
			text.Update = function(object,dt)
				txt[2] = "SFX Volume: "..self.element[i]:GetValue().."%"
				object:SetText(txt)
			end
			text:SetShadow(true)
			text:SetPos(8,(i*45))
			self.element[i]:SetValue(oSoundManager:getVolumeSFX()*100)
		end
	end

	local btn = loveframes.Create("button", self.frame)
	btn:SetFont(loveframes.fonts["punkboy18"])
	btn:SetPos(w/2-62,h-40)
	btn:SetText("Back")
	btn:SetSize(60,30)
	btn.OnClick = function(object,x,y)
		self:onQuit()
	end
	btn.OnMouseEnter = onmouse
	table.insert(self.element,btn)

	btn = loveframes.Create("button", self.frame)
	btn:SetFont(loveframes.fonts["punkboy18"])
	btn:SetPos(w/2+2,h-40)
	btn:SetText("Save")
	btn:SetSize(60,30)
	btn.OnClick = function(object,x,y)
		oSoundManager:setVolumeMaster(self.element[1]:GetValue()/100)
		oSoundManager:setVolumeMusic(self.element[2]:GetValue()/100)
		oSoundManager:setVolumeSFX(self.element[3]:GetValue()/100)
		Game.Config:Save()
		self:onQuit()
	end
	btn.OnMouseEnter = onmouse
	table.insert(self.element,btn)
end

function cUISoundMenu:show(b)
	if (b == true) then
		if not (self.loaded) then
			self:initFrame()
			self.loaded = true
		end
	end
	cUIScriptWnd.show(self,b)
end
