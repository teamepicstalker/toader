cMainMenu = Class{__includes={cUIScriptWnd}}
function cMainMenu:init(...)
	cUIScriptWnd.init(self,...)

	-- create frame and internals
	self:initFrame()

	-- Create SubMenus

	-- Graphics Menu
	self.graphicsMenu = cUIGraphics(self,"graphicsMenu")

	-- Sound Menu
	self.soundMenu = cUISoundMenu(self,"soundMenu")

	-- Keymapping Menu
	self.keymapMenu = cUIKeymapping(self,"keymapMenu")

	-- Highscore List
	self.highScoreMenu = cUIHighScore(self,"highScoreMenu")

	-- MsgBox Quit Game
	self.msgBoxQuit = cUIMsgBox(self,"msgBoxQuit","Exit Game?")
	self.msgBoxQuit.onYes = function(object,x,y) love.event.quit() end
	self.msgBoxQuit.onNo = function(object,x,y) self.msgBoxQuit:onQuit() end
end

function cMainMenu:initFrame()
	-- Setup default OnMouseEnter
	local onmouse = function(object)
		oSoundManager:playSFX("mouseover")
	end

	-- Main Menu Frame
	self.frame:SetDraggable(false)
	self.frame:SetScreenLocked(true)
	self.frame:SetName("Main Menu")
	self.frame:ShowCloseButton(false)
	self.frame:SetSize(200, 160)
	self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)

	-- Continue Button
	local button = loveframes.Create("button", self.frame)
	button:SetFont(loveframes.fonts["punkboy22"])
	button:SetPos(0,0)
	button:SetSize(200,40)
	button:SetText("Continue")
	button.OnClick = function(object, x, y)
		self:onQuit()
	end
	button.OnMouseEnter = onmouse

	table.insert(self.element,button)

	-- Quit Button
	button = loveframes.Create("button", self.frame)
	button:SetFont(loveframes.fonts["punkboy22"])
	button:SetPos(0,40)
	button:SetSize(200,40)
	button:SetText("Exit Game")
	button.OnClick = function(object, x, y)
		self.msgBoxQuit:show(true)
	end
	button.OnMouseEnter = onmouse
	table.insert(self.element,button)

	-- Keymap Button
	button = loveframes.Create("button", self.frame)
	button:SetFont(loveframes.fonts["punkboy22"])
	button:SetPos(0,80)
	button:SetSize(200,40)
	button:SetText("Keymapping")
	button.OnClick = function(object,x,y)
		self.keymapMenu:show(true)
	end
	button.OnMouseEnter = onmouse
	table.insert(self.element,button)

	-- Graphics Button
	button = loveframes.Create("button", self.frame)
	button:SetFont(loveframes.fonts["punkboy22"])
	button:SetPos(0,120)
	button:SetSize(200,40)
	button:SetText("Graphics")
	button.OnClick = function(object,x,y)
		self.graphicsMenu:show(true)
	end
	button.OnMouseEnter = onmouse
	table.insert(self.element,button)

	-- Sound Button
	button = loveframes.Create("button", self.frame)
	button:SetFont(loveframes.fonts["punkboy22"])
	button:SetPos(0,160)
	button:SetSize(200,40)
	button:SetText("Sound")
	button.OnClick = function(object,x,y)
		self.soundMenu:show(true)
	end
	button.OnMouseEnter = onmouse
	table.insert(self.element,button)

	-- HighScore Button
	button = loveframes.Create("button", self.frame)
	button:SetFont(loveframes.fonts["punkboy22"])
	button:SetPos(0,200)
	button:SetSize(200,40)
	button:SetText("HighScores")
	button.OnClick = function(object,x,y)
		self.highScoreMenu:show(true)
	end
	button.OnMouseEnter = onmouse
	table.insert(self.element,button)
end

function cMainMenu:show(b)
	cUIScriptWnd.show(self,b)
	if (b == true and self:isShown(true)) then
		Game.isPaused = true
	else
		Game.isPaused = false
		Game.updateOnPause = false
	end
end

function cMainMenu:onKeyPressed(alias,key,code,bRepeat)
	if (alias == "pause" and not self:isShown()) then
		self:show(true)
		return
	end
	cUIScriptWnd.onKeyPressed(self,alias,key,code,bRepeat)
end
