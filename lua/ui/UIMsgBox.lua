cUIMsgBox = Class{__includes={cUIScriptWnd}}
function cUIMsgBox:init(owner,name,msg,typ)
	cUIScriptWnd.init(self,owner,name)
	self.msg = msg
	self.type = typ
	self.w = 275
	self.h = 115
end

function cUIMsgBox:initFrame()
	-- Setup default OnMouseEnter
	local onmouse = function(object)
		oSoundManager:playSFX("mouseover")
	end

	local w,h = self.w,self.h

	-- Create Frame for MsgBox
	self.frame:SetName("")
	self.frame:SetSize(w, h)
	self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
	self.frame.OnClose = function(object)
		self:onQuit()
	end

	local message = {}
	message[1] = { color={255,255,255,255}, font=loveframes.fonts["punkboy18"] }
	message[2] = self.msg

	local txt = loveframes.Create("text", self.frame)
	self.text = txt
	txt:SetText(message)
	txt:SetX(w/2,true)
	txt:SetY(30)

	local btn = loveframes.Create("button", self.frame)
	btn:SetFont(loveframes.fonts["punkboy18"])
	btn:SetPos(5,h-35)
	btn:SetX(w/2-15,true)
	if (self.type == 1) then
		btn:SetText("OK")
		btn:SetPos(w/2-20,h-35)
	else
		btn:SetText("Yes")
	end
	btn:SetSize(50,30)
	btn.OnClick = function(...) self.onYes(...) end

	btn.OnMouseEnter = onmouse
	table.insert(self.element,btn)

	if (self.type == nil) then
		btn = loveframes.Create("button", self.frame)
		btn:SetFont(loveframes.fonts["punkboy18"])
		btn:SetPos(70,h-35)
		btn:SetX(w/2+40,true)
		btn:SetText("No")
		btn:SetSize(50,30)
		btn.OnClick = function(...) self.onNo(...) end
		btn.OnMouseEnter = onmouse
		table.insert(self.element,btn)
	end
end

function cUIMsgBox:SetText(msg)
	if (self.text) then
		self.text:SetText(msg)
	end
end

function cUIMsgBox:show(b)
	if (b == true) then
		if not (self.loaded) then
			self:initFrame()
			self.loaded = true
		end
	end
	cUIScriptWnd.show(self,b)
end
