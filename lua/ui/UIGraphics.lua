cUIGraphics = Class{__includes={cUIScriptWnd}}
function cUIGraphics:init(...)
	cUIScriptWnd.init(self,...)
	-- MsgBox Graphics Restart
	self.GraphicsmsgBoxOK = cUIMsgBox(self,"GraphicsmsgBoxOK","Changes require a restart!",1)
	self.GraphicsmsgBoxOK.onYes = function(object,x,y) love.event.quit() end
end

function cUIGraphics:initFrame()
	-- Setup default OnMouseEnter
	local onmouse = function(object)
		oSoundManager:playSFX("mouseover")
	end

	local w,h,padding,align = 500, 370, 35, 55

	-- Create Frame for Graphic Menu
	self.frame:SetName("Graphics Options")
	self.frame:SetSize(w,h)
	self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
	self.frame.OnClose = function(object)
		self:onQuit()
	end

	local txt = {}
	txt[1] = { color = {240, 240, 240,255}, font = loveframes.fonts[14] }

	-- Resolution Multi-choice
	local text = loveframes.Create("text",self.frame)
	text:SetShadow(true)
	text:SetPos(15,align+3)
	txt[2] = "Resolution:"
	text:SetText(txt)

	local btn = loveframes.Create("multichoice",self.frame)
	self.resolutionMultibox = btn
	btn:SetPos(100,align)
	btn:SetListHeight(100)

	align = align + padding

	-- MSAA Multi-choice
	text = loveframes.Create("text",self.frame)
	text:SetShadow(true)
	text:SetPos(15,align+3)
	txt[2] = "MSAA:"
	text:SetText(txt)

	btn = loveframes.Create("multichoice",self.frame)
	self.MSAAMultibox = btn
	btn:SetPos(100,align)

	align = align + padding

	-- Display Multi-choice
	text = loveframes.Create("text",self.frame)
	text:SetShadow(true)
	text:SetPos(15,align+3)
	txt[2] = "Display:"
	text:SetText(txt)

	btn = loveframes.Create("multichoice",self.frame)
	self.DisplayMultibox = btn
	btn:SetPos(100,align)

	align = align + padding

	-- Scale Slider
	btn = loveframes.Create("slider", self.frame)
	self.ScaleSlider = btn
	btn:SetDecimals(1)
	btn:SetPos(100,align)
	btn:SetWidth(70)
	btn:SetMinMax(1,2)
	btn.internals[1].OnMouseEnter = onmouse
	btn.OnValueChanged = function(object,val)
		oCamera.scale = val
		oCamera:resetViewport()
	end

	text = loveframes.Create("text",self.frame)
	text:SetShadow(true)
	text:SetPos(15,align+3)
	text:SetAlwaysUpdate(true)
	text.Update = function(object,dt)
		txt[2] = "Scale: "..self.ScaleSlider:GetValue().."%"
		object:SetText(txt)
	end

	align = align + padding

	-- Checkboxes
	local t = {
				["fullscreen"] = "Fullscreen:",
				["borderless"] = "Borderless:",
				["vsync"] = "V-Sync:"
	}

	for k, v in pairs(t) do
		btn = loveframes.Create("checkbox",self.frame)
		self["checkBox_"..k] = btn
		btn:SetPos(100,align)

		txt[2] = v
		btn = loveframes.Create("text",self.frame)
		btn:SetPos(15,align)
		btn:SetText(txt)
		btn:SetShadow(true)

		align = align + padding
	end

	-- Back Button
	btn = loveframes.Create("button", self.frame)
	btn:SetFont(loveframes.fonts["punkboy18"])
	btn:SetPos(w/2-62,h-40)
	btn:SetText("Back")
	btn:SetSize(60,30)
	btn.OnClick = function(object,x,y)
		self:onQuit()
		oCamera.scale = Game.Config:r_number("graphics","scale") or 1
		oCamera:resetViewport()
	end
	btn.OnMouseEnter = onmouse
	table.insert(self.element,btn)

	-- Save Button
	btn = loveframes.Create("button", self.frame)
	btn:SetFont(loveframes.fonts["punkboy18"])
	btn:SetPos(w/2+2,h-40)
	btn:SetText("Save")
	btn:SetSize(60,30)
	btn.OnClick = function(object,x,y)
		local require_restart = false
		local w,h,flags = love.window.getMode()

		-- Set Resolution
		local choice = self.resolutionMultibox:GetChoice()
		choice = choice and str_explode(choice,"x")
		choice[1] = choice[1] and tonumber(choice[1])
		choice[2] = choice[2] and tonumber(choice[2])
		if (choice and choice[1] and choice[2]) then
			if (w ~= choice[1]) then
				w = choice[1]
				require_restart = true
			end

			if (h ~= choice[2]) then
				h = choice[2]
				require_restart = true
			end
		end
		Game.Config:SetValue("graphics","w",w)
		Game.Config:SetValue("graphics","h",h)

		-- Set MSAA
		choice = self.MSAAMultibox:GetChoice()
		choice = tonumber(choice) or 0
		if (flags.msaa ~= choice) then
			flags.msaa = choice
		end
		Game.Config:SetValue("graphics","msaa",flags.msaa)

		-- Set Display
		choice = tonumber(self.DisplayMultibox:GetChoice())
		if (choice ~= flags.display) then
			flags.display = choice
		end
		Game.Config:SetValue("graphics","display",choice)

		-- Checkbox
		local t = { ["fullscreen"] = true, ["borderless"] = false, ["vsync"] = false }

		for k,v in pairs(t) do
			choice = self["checkBox_"..k]:GetChecked()
			if (choice ~= flags[k]) then
				flags[k] = choice
				if (v == true) then
					require_restart = true
				end
			end
			Game.Config:SetValue("graphics",k,flags[k])
		end

		-- Scale
		Game.Config:SetValue("graphics","scale",oCamera.scale)

		Game.Config:Save()

		if (require_restart ~= true) then
			flags.centered = true
			love.window.setMode(w,h,flags)
			oCamera:resetViewport()
			self:onQuit()
		else
			self.GraphicsmsgBoxOK:show(true)
		end
	end
	btn.OnMouseEnter = onmouse
	table.insert(self.element,btn)
end

function cUIGraphics:setupValues()
	-- Resolution
	local resolution = {
		"1024x768",
		"1280x1024",
		"1280x720",
		"1366x768",
		"1600x900",
		"1920x1080",
		"2560x1440",
		"3840x2160",
		"1280x800",
		"1440x900",
		"1680x1050",
		"1920x1200",
		"2560x1600"
	}

	for i=1, #resolution do
		self.resolutionMultibox:AddChoice(resolution[i])
	end

	-- MSAA
	local MSAA = {
		"0",
		"2",
		"4",
		"8",
		"16"
	}

	for i=1,#MSAA do
		self.MSAAMultibox:AddChoice(MSAA[i])
	end

	-- Display
	local count = love.window.getDisplayCount()
	for i=1,count do
		self.DisplayMultibox:AddChoice(i)
	end

	-- Scale
	self.ScaleSlider:SetValue(oCamera.scale)
end

function cUIGraphics:refresh()
	local w,h,flags = love.window.getMode()

	self.resolutionMultibox:SetChoice(w.."x"..h)
	self.MSAAMultibox:SetChoice(flags.msaa or 0)
	self.DisplayMultibox:SetChoice(flags.display or 1)
	self.ScaleSlider:SetValue(oCamera.scale)

	-- Checkbox
	local t = { "fullscreen", "borderless", "vsync" }

	for i=1, #t do
		self["checkBox_"..t[i]]:SetChecked(flags[t[i]])
	end
end

function cUIGraphics:show(b)
	if (b == true) then
		if not (self.loaded) then
			self:initFrame()
			self:setupValues()
			self.loaded = true
		end
		self:refresh()
	end
	cUIScriptWnd.show(self,b)
end
