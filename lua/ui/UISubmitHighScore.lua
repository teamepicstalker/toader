cUISubmitHighScore = Class{__includes={cUIScriptWnd}}
function cUISubmitHighScore:init(...)
	cUIScriptWnd.init(self,...)
	-- MsgBox Submit
	self.msgBoxSubmit = cUIMsgBox(self,"msgBoxSubmit","Submit HighScore?")
	self.msgBoxSubmit.onYes = function(object,x,y) 
		self.msgBoxSubmit:onQuit() 
		self:submit()
		--self:onQuit()
	end
	self.msgBoxSubmit.onNo = function(object,x,y) self.msgBoxSubmit:onQuit() end
	
	-- MsgBox Illegal Name
	self.IllegalmsgBoxOK = cUIMsgBox(self,"IllegalmsgBoxOK","Illegal name!\nMust be atleast 5 characters\nand not contain any illegal characters.",1)
	self.IllegalmsgBoxOK.w = 400
	self.IllegalmsgBoxOK.h = 155
	self.IllegalmsgBoxOK.onYes = function(object,x,y) self.IllegalmsgBoxOK:onQuit() end
end 

function cUISubmitHighScore:initFrame()
	-- Setup default OnMouseEnter
	local onmouse = function(object)
		oSoundManager:playSFX("mouseover")
	end

	local w,h = 300, 275
	-- Create Frame for Sound Menu
	self.frame:SetName("Submit HighScore")
	self.frame:SetSize(w,h)
	self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
	self.frame.OnClose = function(object)
		self:onQuit()	
	end
	
	local txt = {}
	txt[1] = { color = {240, 240, 240,255}, font = loveframes.fonts[14] }
	
	local text = loveframes.Create("text", self.frame)
	text:SetPos(8, 35)
	txt[2] = "Name:"
	text:SetText(txt)
	text:SetShadow(true)
	
	local textinput = loveframes.Create("textinput", self.frame)
	self.inputName = textinput
	textinput:SetPos(75, 30)
	textinput:SetWidth(180)
	textinput.Update = function(object)
		local len = object:GetText():len()
		if (len > 30) then 
			object:SetText(object:GetText():sub(1,len-1))
		end
	end
	
	text = loveframes.Create("text", self.frame)
	self.TextHighScore = text
	text:SetPos(8, 70)
	txt[2] = "HighScore:"
	text:SetText(txt)
	text:SetShadow(true)
	
	-- Participate Online checkbox
	txt[2] = "Participate Online"
	local button = loveframes.Create("text",self.frame)
	button:SetPos(30,h-70)
	button:SetText(txt)
	button:SetShadow(true)

	button = loveframes.Create("checkbox",self.frame)
	self.checkBox_participate = button
	button:SetPos(8,h-70)
	
	-- set default to true
	local b = Game.Config:r_bool("online","participate")
	button:SetChecked(b == nil or b == true)
	Game.Config:SetValue("online","participate",b == nil or b == true)
	
	button.OnChanged = function(object,val)
		--self:refreshList()
		Game.Config:SetValue("online","participate",val)
		if (val == true) then
			Game.HighScore = Game:getNextHighScore()
		else
			Game.HighScore = Game.Config:r_number("savegame","high_score") or 0
		end
	end
	
	local function valid(str)
		return #str >= 5 and #str <= 30 and str:find('^[%-%.%w_]+$') ~= nil
	end

	button = loveframes.Create("button", self.frame)
	button:SetPos(w/2-80, h-35)
	button:SetSize(80,30)
	button:SetText("Done")
	button:SetFont(loveframes.fonts["punkboy18"])
	button.OnClick = function(object)
		if (valid(textinput:GetText())) then
			self.msgBoxSubmit:show(true)
		else 
			self.IllegalmsgBoxOK:show(true)
			self.IllegalmsgBoxOK:SetText("Illegal name!\nMust be atleast 5 characters\nand not contain any illegal characters.")
			self.IllegalmsgBoxOK.onYes = function(object,x,y) self.IllegalmsgBoxOK:onQuit() end
		end
	end
	
	button = loveframes.Create("button", self.frame)
	button:SetPos(w/2, h-35)
	button:SetSize(80,30)
	button:SetText("Cancel")
	button:SetFont(loveframes.fonts["punkboy18"])
	button.OnClick = function()
		self:onQuit()
	end
end 

function cUISubmitHighScore:setHighScore()
	if (self.TextHighScore) then
		local txt = {}
		txt[1] = { color = {240, 240, 240,255}, font = loveframes.fonts[14] }
		txt[2] = "HighScore: " .. self.score
		self.TextHighScore:SetText(txt)
	end
end

-- To make cheating much more difficult
function cUISubmitHighScore:validateHighscore(score)
	if (score <= (Game.VictoryCount+1) * 6336) then 
		return true 
	end 
	return false
end 

function cUISubmitHighScore:submit()
	if not (self:validateHighscore(self.score)) then 
		self.IllegalmsgBoxOK:show(true)
		self.IllegalmsgBoxOK:SetText("Unable to validate HighScore!")
		self.IllegalmsgBoxOK.onYes = function(object,x,y) 
			self.IllegalmsgBoxOK:onQuit() 
			self:onQuit()
		end
		return	
	end

	-- Player ID is used to keep track of the same player when submitting HighScore
	local playerid = Game.Config:r_number("online","playerid")
	if not (playerid) then
		local t = {}
		local res, code, response_headers, status = http.request{
			url = "http://www.epicstalker.com/toader/score_script.php?gameid=1&topid=true",
			method = "GET",
			sink = ltn12.sink.table(t)
		}
		
		playerid = code == 200 and tonumber(t[1])
		if not (playerid) then 
			self.IllegalmsgBoxOK:show(true)
			self.IllegalmsgBoxOK:SetText("Unable to submit HighScore")
			self.IllegalmsgBoxOK.onYes = function(object,x,y) 
				self.IllegalmsgBoxOK:onQuit() 
				self:onQuit()
			end
			return
		end
	
		-- increment the playerid
		playerid = playerid + 1
	end
	
	local oldkey = Game.Config:r_string("online","key") or "nil"
	local playername = self.inputName:GetText()
	local score = self.score
	local secretkey = md5.sumhexa(playername..score.."tkoka8dr6")

	local t = {}
	local res, code, response_headers, status = http.request{
		url = strformat("http://www.epicstalker.com/toader/score_script.php?gameid=1&playerid=%s&playername=%s&score=%s&code=%s&key=%s",playerid,playername,score,secretkey,oldkey),
		method = "GET",
		sink = ltn12.sink.table(t)
	}
	
	if not (t[1]) then
		self.IllegalmsgBoxOK:show(true)
		self.IllegalmsgBoxOK:SetText("Unable to submit HighScore")
		self.IllegalmsgBoxOK.onYes = function(object,x,y) 
			self.IllegalmsgBoxOK:onQuit() 
			self:onQuit()
		end
		return
	end
	
	t = str_explode(t[1],"|")
	
	if (t[2]) then
		playerid = tonumber(t[2])
	end

	t[1] = trim(t[1])
	--log("code=%s sink=%s playerid=%s",code,t[1],t[2])
	if (code == 200 and t[1] == "success") then
		Game.Config:SetValue("online","key",secretkey)
		Game.Config:SetValue("online","playerid",playerid)
		Game.Config:Save()
		self.IllegalmsgBoxOK:show(true)
		self.IllegalmsgBoxOK:SetText("HighScore was submitted!")
		self.IllegalmsgBoxOK.onYes = function(object,x,y) 
			self.IllegalmsgBoxOK:onQuit() 
			self:onQuit()
		end
	end
end 

function cUISubmitHighScore:show(b)
	if (b == true and Game.Config:r_bool("online","participate") ~= true) then 
		return 
	end
	if (b == true) then
		if not (self.loaded) then 
			self:initFrame()
			self.loaded = true
		end
		self.score = Game.Score
		self:setHighScore()
	end
	cUIScriptWnd.show(self,b)
	if (b == true and self:isShown(true)) then
		Game.isPaused = true
	else
		Game.isPaused = false
		Game.updateOnPause = false
	end
end