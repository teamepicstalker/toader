cUIHighScore = Class{__includes={cUIScriptWnd}}
function cUIHighScore:init(...)
	cUIScriptWnd.init(self,...)
end 

function cUIHighScore:initFrame()
	-- Setup default OnMouseEnter
	local onmouse = function(object)
		oSoundManager:playSFX("mouseover")
	end

	local w,h = 670, 370
	-- Create Frame for Sound Menu
	self.frame:SetName("Top HighScores")
	self.frame:SetSize(w,h)
	self.frame:CenterWithinArea(0,0,windowWidth,windowHeight)
	self.frame.OnClose = function(object)
		self:onQuit()	
	end

	local txt = {}
	txt[1] = { color = {240, 240, 240,255}, font = loveframes.fonts[14] }
	
	-- Create a List
	local list = loveframes.Create("list", self.frame)
	self.HighScoreList = list
	list:SetPos(5, 30)
	list:SetSize(660, 270)
	list:SetPadding(5)
	list:SetSpacing(5)
	list:EnableHorizontalStacking(true)
	
	-- Participate Online checkbox
	txt[2] = "Participate Online"
	local btn = loveframes.Create("text",self.frame)
	btn:SetPos(30,308)
	btn:SetText(txt)
	btn:SetShadow(true)
		
	btn = loveframes.Create("checkbox",self.frame)
	self.checkBox_participate = btn
	btn:SetPos(8,305)
	
	-- set default to true
	local b = Game.Config:r_bool("online","participate")
	btn:SetChecked(b == nil or b == true)
	Game.Config:SetValue("online","participate",b == nil or b == true)
	
	btn.OnChanged = function(object,val)
		--self:refreshList()
		Game.Config:SetValue("online","participate",val)
		if (val == true) then
			Game.HighScore = Game:getNextHighScore()
		else
			Game.HighScore = Game.Config:r_number("savegame","high_score") or 0
		end
	end
		
	-- Back Button
	btn = loveframes.Create("button", self.frame)
	btn:SetFont(loveframes.fonts["punkboy18"])
	btn:SetPos(w/2-30,330)
	btn:SetText("Back")
	btn:SetSize(60,30)
	btn.OnClick = function(object,x,y)
		self:onQuit()
	end
	btn.OnMouseEnter = onmouse
	table.insert(self.element,btn)
end

function cUIHighScore:refreshList()
	self.HighScoreList:Clear()
	
	if (Game.Config:r_bool("online","participate") ~= true) then 
		return 
	end
	
	local t = {}
	local res, code, response_headers, status = http.request{
		url = [[http://www.epicstalker.com/toader/index.php]],
		headers = { ["Content-Type"] = "application/x-www-form-urlencoded" },
		method = "GET",
		sink = ltn12.sink.table(t)
	}
	
	if not (code == 200) then 
		return
	end
	
	local data = str_explode(t[1],"|")
	
	local n = 1
	local size = #data/3
	for i=1,size do
	
		local button = loveframes.Create("button")
		button:SetSize(50,30)
		button:SetText(i)
		button:SetClickable(false)
		button.CheckHover = function() end
		button:SetFont(loveframes.fonts[14])
		self.HighScoreList:AddItem(button)
		
		button = loveframes.Create("button")
		button:SetSize(200,30)
		button:SetText(data[n] or "")
		button:SetClickable(false)
		button.CheckHover = function() end
		button:SetFont(loveframes.fonts[14])
		self.HighScoreList:AddItem(button)
		n = n + 1

		button = loveframes.Create("button")
		button:SetSize(200,30)
		button:SetText(data[n] or "")
		button:SetClickable(false)
		button.CheckHover = function() end
		button:SetFont(loveframes.fonts[14])
		self.HighScoreList:AddItem(button)
		n = n + 1

		button = loveframes.Create("button")
		button:SetSize(150,30)
		button:SetText(data[n] or "")
		button:SetClickable(false)
		button.CheckHover = function() end
		button:SetFont(loveframes.fonts[14])
		self.HighScoreList:AddItem(button)
		n = n + 1
	end
	
end 

function cUIHighScore:show(b)
	if (b == true) then
		if not (self.loaded) then 
			self:initFrame()
			self.loaded = true
		end
		self:refreshList()
	end
	cUIScriptWnd.show(self,b)
end