cHud = Class{}
function cHud:init()
	-- Current Score UI
	self.textScore = {}
	self.textScore[1] = {color = {255,255,255,255}, font = loveframes.fonts["lunch32"]}

	self.uiScore = loveframes.Create("text")
	self.uiScore:SetPos((windowWidth/2)-5,64,true)
	self.uiScore:SetShadow(true)
	self.uiScore:SetIgnoreState(true)

	-- HighScore UI
	self.textHighScore = {}
	self.textHighScore[1] = {color = {255,255,255,255}, font = loveframes.fonts["lunch32"]}

	self.uiHighScore = loveframes.Create("text")
	self.uiHighScore:SetShadow(true)
	self.uiHighScore:SetIgnoreState(true)

	RegisterScriptCallback("onUpdate",self.onUpdate,self)
end

function cHud:onUpdate(dt)
	AnimationLib["lives"].animation:update(dt)

	self.textScore[2] = tostring(Game.Score)
	self.uiScore:SetText(self.textScore)
	self.uiScore:SetPos((windowWidth/2),74,true)

	self.textHighScore[2] = "HighScore: "..tostring(Game.HighScore)
	self.uiHighScore:SetText(self.textHighScore)
	self.uiHighScore:SetPos((windowWidth/4)-5,20,true)
end

function cHud:draw()
	local x = (windowWidth/2)-(64*(Game.ToadLives/2))
	local a = AnimationLib["lives"]
	for i=1,Game.ToadLives do
		a.animation:draw(a.image,x+(64*(i-1)),0)
	end
end
