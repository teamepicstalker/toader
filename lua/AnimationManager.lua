AnimationLib = require 'lua.AnimationLib'
cAnimationManager = Class{}
function cAnimationManager:init()
	self.spriteSheet = {}
	for k,v in pairs(AnimationLib) do
		local name = v.path
		if not (self.spriteSheet[name]) then
			self.spriteSheet[name] = {}
			loader.newImage(self.spriteSheet[name],"image",name)
		end 
	end
	RegisterScriptCallback("onLoaderFinished",self.onLoaderFinished,self)
end

function cAnimationManager:onLoaderFinished()
	for k,v in pairs(AnimationLib) do
		local name = v.path
		if not (self.spriteSheet[name][v.width..v.height]) then 
			self.spriteSheet[name][v.width..v.height] = cAnimation.newGrid(v.width,v.height,self.spriteSheet[name].image:getWidth(),self.spriteSheet[name].image:getHeight())
		end
		v.image = self.spriteSheet[name].image
		v.image:setFilter("nearest","nearest",1)
		v.animation = cAnimation.newAnimation(self.spriteSheet[name][v.width..v.height](unpack(v.frames)),v.duration)
		if (v.flipH) then
			v.animation = v.animation:flipH()
		end
		if (v.flipV) then 
			v.animation = v.animation:flipV()
		end		
	end
	self.spriteSheet = nil
end