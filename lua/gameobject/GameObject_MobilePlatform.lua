cMobilePlatform = Class{__includes={cGameObject}}
function cMobilePlatform:init(pos,properties)
	cGameObject.init(self)
	
	self.position = pos
	self.startPos = pos:clone()
	self.properties = table.copy(properties)
	
	self.properties.speed = (self.properties.speed or 1) * Game.difficultySpeedMult
	
	-- clamp speed
	local r = self.properties.rate > 0 and 192/self.properties.rate or self.properties.speed
	self.properties.speed = math.clamp(self.properties.speed,r,600)
			
	-- Sprite Manager
	if not (self.sprite) then
		self.sprite = cSpriteManager(self.id,pos,0,"lilypad_idle")
	else  -- sprite might already exist if we are recycled
		self.sprite.id = self.id
		self.sprite.position = pos
		self.sprite:setState("lilypad_idle")
	end
	
	-- Pathing Index and Direction
	self.waypointIndex = 1
	self.waypointInc = 1
	
	-- Set Sprite according to spawn properties 
	if (self.properties.sprite) then
		local spr = self.properties.sprite[math.random(#self.properties.sprite)]
		self.sprite:setState(spr)
	end
end
function cMobilePlatform:update(dt)
	cGameObject.update(self,dt)
	
	if not (self.online) then 
		return 
	end
	
	-- outside of map, so we must tell level to recycle/destroy us and unregister as a game object
	if (self.position.x < -128 or self.position.x > mapWidth) then 
		CurrentLevel:recycle(self)
		return
	end
	
	local way = self.properties.path[self.waypointIndex]
	if (way) then 
		local x = self.startPos.x+way.x
		local y = self.startPos.y+way.y
		
		if (y < self.position.y) then 
			self.position.y = self.position.y - 1 * self.properties.speed * dt
		elseif (y > self.position.y) then
			self.position.y = self.position.y + 1 * self.properties.speed * dt
		end
		if (x < self.position.x) then 
			self.position.x = self.position.x - 1 * self.properties.speed * dt
		elseif (x > self.position.x) then
			self.position.x = self.position.x + 1 * self.properties.speed * dt
		end
		
		if (x+3 < self.position.x+6 and y+3 < self.position.y+6) then 
			if (self.position.x+3 < x+6 and self.position.y+3 < y+6) then
				if (self.waypointIndex + self.waypointInc > #self.properties.path) then
					if (self.reverse) then
						self.waypointIndex = #self.properties.path
						self.waypointInc = -self.waypointInc
					else 
						self.waypointIndex = 1
					end
				elseif (self.waypointIndex + self.waypointInc < 1) then 
					self.waypointIndex = 1
					self.waypointInc = -self.waypointInc
				else 
					self.waypointIndex = self.waypointIndex + self.waypointInc
				end
			end
		end
	end
	
	if (oPlayer.isDead) then
		return 
	end
	
	-- attach player to self if he is on the same grid
	if (self.attachedID) then
		local goAttached = CurrentLevel:objectByID(self.attachedID)
		if (goAttached) then
			if (goAttached:inside(self)) then
				goAttached:attach(self,self.attachOffset)
				if (self.sprite:getState() == "lilypad_idle_vanish") then 
					self.sprite:setState("lilypad_fade")
				elseif (self.sprite:getState() == "log_small_idle_vanish") then
					self.sprite:setState("log_small_fade")
				end
			else 
				goAttached:detach()
			end
		end
	elseif (oPlayer.lastHolderID ~= self.id and not oPlayer.holderID and oPlayer:inside(self) and self.sprite:getState() ~= "lilypad_fade") then
		local x = math.round((oPlayer.position.x-self.position.x)/64)*64
		if (x < 0) then 
			x = 0
		elseif (x > self.boundingBox.w-62) then 
			x = self.boundingBox.w-62
		end
		self.attachOffset = Vector2(x,0)
		oPlayer:attach(self,self.attachOffset)
		self.attachedID = oPlayer.id
	end
end

function cMobilePlatform:animation_callback(sprMgr,animation,count)
	cGameObject.animation_callback(self,sprMgr,animation,count)
	local function dumpPlayer()
		local goAttached = self.attachedID and CurrentLevel:objectByID(self.attachedID)
		self.attachedID = nil
		if (goAttached) then 
			goAttached:detach()
		end
		self:switchOffline()	
	end 
	
	if (sprMgr:getState() == "lilypad_fade") then
		dumpPlayer()
		sprMgr:setState("lilypad_idle_vanish")
	elseif (sprMgr:getState() == "log_small_fade") then 
		dumpPlayer()
		sprMgr:setState("log_small_idle_vanish")
	end
end
