cPlayer = Class{__includes={cGameObject}}
function cPlayer:init(pos)
	cGameObject.init(self)

	self.class = "cPlayer"
	self.alwaysOnline = true

	-- pos Vector2
	self.position = pos

	-- Sprite Data
	self.sprite = cSpriteManager(self.id,pos,0,"idle_front")

	-- Vector to hold current input direction
	self.__delta = Vector2(0,0)

	-- Absolute grid position.
	self.grid_position = pos:clone()

	-- Properties like speed
	self.properties = {}
	
	-- The speed position lerps to grid_position
	self.properties.speed = 25

	-- Mainly used to keep track when player moves forward or backward so we can animate properly
	self.last_dir = 0

	-- Register for callbacks
	RegisterScriptCallback("onGameOver",self.onGameOver,self)
end

function cPlayer:onGameOver()
	oSoundManager:playSFX("gameover")
end

function cPlayer:checkMovement(dt)
	if (self.isMoving == true and love.timer.getTime() > self.hopTime) then
		if (self.last_dir == 0) then
			self.sprite:setState("idle_front",true)
		else
			self.sprite:setState("idle_back",true)
		end
		if not (self.holderID) then
			self.lastHolderID = nil
		end

		local x = self.grid_position.x
		self.grid_position.x = math.round(self.grid_position.x/64)*64
		self.grid_position.y = math.round(self.grid_position.y/64)*64

		if (x ~= self.grid_position.x) then
			if (self.last_dir == 0) then
				self.sprite:setState("hop_front_side",true)
			else
				self.sprite:setState("hop_back_side",true)
			end
		end

		self.isMoving = false
	end

	-- tween current position to absolute grid position for smooth animation
	local goHolder = self.holderID and CurrentLevel:objectByID(self.holderID)
	if (goHolder) then
		self.position.x = self.position.x - ((self.position.x - (goHolder.position.x+goHolder.attachOffset.x)) * 25 * dt)
		self.position.y = self.position.y - ((self.position.y - (goHolder.position.y+goHolder.attachOffset.y)) * 25 * dt)
	else
		self.position.x = self.position.x - ((self.position.x - self.grid_position.x) * self.properties.speed * dt)
		self.position.y = self.position.y - ((self.position.y - self.grid_position.y) * self.properties.speed * dt)
	end
end

function cPlayer:update(dt)
	cGameObject.update(self,dt)
	if (self.isDead) then
		return
	end
	self.sprite:setLayer("Dynamic"..math.round(self.position.y/64))
	self:checkMovement(dt)
end

function cPlayer:attach(obj,offset)
	if (self.isDead) then
		return
	end

	self.grid_position.x = math.round(obj.position.x/64)*64 + offset.x
	self.grid_position.y = math.round(obj.position.y/64)*64 + offset.y

	if (self.grid_position.x < 0) then
		self.grid_position.x = 0
	elseif (self.grid_position.x > mapWidth-64) then
		self.grid_position.x = mapWidth-64
	end

	if (self.position.x < 0) then
		self.position.x = 0
	elseif (self.position.x > mapWidth-64) then
		self.position.x = mapWidth-64
	end

	self.holderID = obj.id
end

function cPlayer:makeDead()
	if not (DEBUG) then
		self.isDead = true
	end
end

function cPlayer:setPosition(x,y)
	self.position.x = x
	self.position.y = y
	self.grid_position.x = (x/64)*64
	self.grid_position.y = (y/64)*64
end

function cPlayer:rebirth()
	self:detach()
	self.last_dir = 0
	self.isDead = false
	self.isMoving = false
	self.sprite.visible = true
	self.sprite:setState("idle_front",true)
end

function cPlayer:netSpawn()
	cGameObject.netSpawn(self)
	oKeybindManager:register("onJoystickAdded",self.onJoystickAdded,self)
	oKeybindManager:registerJoystick(1,"onGamepadPressed",self.onInput,self)
	oKeybindManager:register("onKeyPressed",self.onInput,self)
end

function cPlayer:netDestroy()
	cGameObject.netDestroy(self)
	oKeybindManager:unregister("onJoystickAdded",self)
	oKeybindManager:unregisterJoystick(1,"onGamepadPressed",self)
	oKeybindManager:unregister("onKeyPressed",self)
end

function cPlayer:onJoystickAdded(joy,ind)
	-- let only joystick 1 control player
	if (ind == 1) then
		oKeybindManager:registerJoystick(1,"onGamepadPressed",self.onInput,self)
	end
end

function cPlayer:onInput(alias,key,code,bRepeat)
	if (Game.isPaused) then
		return
	end

	if (self.isDead) then
		return
	end

	if (self.isMoving) then
		return
	end

	local goHolder = self.holderID and CurrentLevel:objectByID(self.holderID)

	-- We move according to input
	-- Player has two positions. The current position (which used to draw sprite) and then the actual grid position
	-- Position will lerp to grid position so that everything looks smooth
	if (alias == "left") and (self.grid_position.x - 64 >= 0) then
		if (goHolder) then
			if (self.last_dir == 0) then
				self.sprite:setState("hop_front_side",true)
			else
				self.sprite:setState("hop_back_side",true)
			end

			if (goHolder.attachOffset.x - 64 >= 0) then
				goHolder.attachOffset.x = goHolder.attachOffset.x - 64
			else
				self.grid_position.x = self.grid_position.x - 64
				self.grid_position.y = self.position.y
				self:detach()
			end
		else
			if (self.last_dir == 0) then
				self.sprite:setState("hop_front",true)
			else
				self.sprite:setState("hop_back",true)
			end
			self.grid_position.x = self.grid_position.x - 64
			self.grid_position.y = self.position.y
		end
		self.isMoving = true
		oSoundManager:playSFX("hop")
		self.hopTime = love.timer.getTime()+0.17
	elseif (alias == "right") and (self.grid_position.x + 64 < mapWidth) then
		if (goHolder) then
			if (self.last_dir == 0) then
				self.sprite:setState("hop_front_side",true)
			else
				self.sprite:setState("hop_back_side",true)
			end

			if (goHolder.attachOffset.x+goHolder.boundingBox.x+64 < goHolder.boundingBox.w) then
				goHolder.attachOffset.x = goHolder.attachOffset.x + 64
			else
				self.grid_position.x = self.grid_position.x + 64
				self.grid_position.y = self.position.y
				self:detach()
			end
		else
			if (self.last_dir == 0) then
				self.sprite:setState("hop_front",true)
			else
				self.sprite:setState("hop_back",true)
			end
			self.grid_position.x = self.grid_position.x + 64
			self.grid_position.y = self.position.y
		end
		self.isMoving = true
		oSoundManager:playSFX("hop")
		self.hopTime = love.timer.getTime()+0.17
	end
	if (alias == "up") and (self.grid_position.y - 64 >= 0) then
		self.grid_position.y = self.grid_position.y - 64
		self.grid_position.x = self.position.x

		self.sprite:setState("hop_front",true)
		self.last_dir = 0
		self:detach()
		self.isMoving = true
		oSoundManager:playSFX("hop")
		self.hopTime = love.timer.getTime()+0.17
	elseif (alias == "down") and (self.grid_position.y + 64 < mapHeight) then
		self.grid_position.y = self.grid_position.y + 64
		self.grid_position.x = self.position.x

		self.sprite:setState("hop_back",true)
		self.last_dir = 1
		self:detach()
		self.isMoving = true
		oSoundManager:playSFX("hop")
		self.hopTime = love.timer.getTime()+0.17
	end
end
function cPlayer:resetBoundingBox()
	if (self.sprite and self.sprite.visible) then
		self.boundingBox.x = 16
		self.boundingBox.y = 8
		self.boundingBox.w = 32
		self.boundingBox.h = 48
	else
		self.boundingBox.x = 0
		self.boundingBox.y = 0
		self.boundingBox.w = 0
		self.boundingBox.h = 0
	end
end
function cPlayer:checkCollision(x1,y1,w1,h1)
	local x2 = self.grid_position.x+self.boundingBox.x
	local y2 = self.grid_position.y+self.boundingBox.y
	return x1 < x2 + self.boundingBox.w and x1 + w1 > x2 and y1 < y2 + self.boundingBox.h and y1 + h1 > y2
end
function cPlayer:animation_callback(sprMgr,animation,count)
	if (self.isMoving == false) and (sprMgr.state == "hop_front_side" or sprMgr.state == "hop_back_side") then
		if (self.last_dir == 0) then
			self.sprite:setState("idle_front",true)
		else
			self.sprite:setState("idle_back",true)
		end
	end
end
