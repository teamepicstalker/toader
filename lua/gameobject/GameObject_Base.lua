cGameObject = Class{}
function cGameObject:init()
	self.online = false
	self.switch_online = true 
	self.switch_offline = true
	
	-- register object and obtain gameObject id
	self.id = CurrentLevel:register(self)
	
	self.name = self.name or tostring(self.id)
	
	self.boundingBox = {x=0, y=0, w=0, h=0}
end
function cGameObject:netSpawn()
	if (self.sprite) then
		self.sprite.visible = true
		local state = self.sprite.state
		self.sprite.state = nil -- so it can reset
		self.sprite:setState(state)
	end
	self:resetBoundingBox()
end
function cGameObject:netDestroy()
	local goAttached = self.attachedID and CurrentLevel:objectByID(self.attachedID)
	self.attachedID = nil
	if (goAttached) then
		goAttached:detach()
	end
	local goHolder = self.holderID and CurrentLevel:objectByID(self.holderID)
	self.holderID = nil
	if (goHolder) then 
		self:detach()
	end
	if (self.sprite) then
		self.sprite:setState(nil)
	end
	self:resetBoundingBox()
end
function cGameObject:destroy()
	local goAttached = self.attachedID and CurrentLevel:objectByID(self.attachedID)
	self.attachedID = nil
	if (goAttached) then
		goAttached:detach()
	end
	if (self.sprite) then
		self.sprite:destroy()
	end
end
function cGameObject:update(dt)
	if (self.online) then
		if (self:canSwitchOffline()) then 
			self:switchOffline()
		end
	else
		if (self:canSwitchOnline()) then 
			self:switchOnline()
		end 
	end
end
function cGameObject:animation_callback(sprMgr,animation,count)

end
function cGameObject:switchOffline()
	self.online = false
	self:netDestroy()
end
function cGameObject:switchOnline()
	self.online = true
	self:netSpawn()
end
function cGameObject:canSwitchOnline()
	if not (self.switch_online) then 
		return false 
	end
	
	if (self.online) then 
		return true 
	end
	
	if (self.alwaysOnline) then 
		return true 
	end
	
	if (math.abs(oCamera.position.x-self.position.x) <= mapWidth+128) then 
		if (math.abs(oCamera.position.y-self.position.y) <= mapHeight) then 
			return true
		end	
	end
	
	return false
end 
function cGameObject:canSwitchOffline()
	if not (self.switch_offline) then 
		return false 
	end
	
	if not (self.online) then 
		return true
	end
	
	return not self:canSwitchOnline()
end
function cGameObject:inside(obj)
	local x1 = obj.position.x+obj.boundingBox.x
	local y1 = obj.position.y+obj.boundingBox.y
	local x2 = self.position.x+self.boundingBox.x
	local y2 = self.position.y+self.boundingBox.y
	return x1 < x2 + self.boundingBox.w and x1 + obj.boundingBox.w > x2 and y1 < y2 + self.boundingBox.h and y1 + obj.boundingBox.h > y2
end
function cGameObject:checkCollision(x1,y1,w1,h1)
	local x2 = self.position.x+self.boundingBox.x
	local y2 = self.position.y+self.boundingBox.y
	return x1 < x2 + self.boundingBox.w and x1 + w1 > x2 and y1 < y2 + self.boundingBox.h and y1 + h1 > y2
end
function cGameObject:draw()
	if not (self.online) then 
		return
	end
	if (DEBUG) then
		love.graphics.rectangle("line",self.position.x+self.boundingBox.x,self.position.y+self.boundingBox.y,self.boundingBox.w,self.boundingBox.h)
		love.graphics.print(strformat("speed=%s rate=%s",self.properties.speed,self.properties.rate),self.position.x,self.position.y)
		if (self.id == 0) then 
			love.graphics.setColor(0, 0, 255, 255)
			love.graphics.rectangle("line",0,oCamera.position.y-576,mapWidth,1152)
			love.graphics.rectangle("line",self.grid_position.x,self.grid_position.y,64,64)
			love.graphics.setColor(255,0,0,255)
		end
	end
end
function cGameObject:resetBoundingBox()
	if (self.sprite and self.sprite.visible) then 
		self.boundingBox.x = 1
		self.boundingBox.y = 1
		self.boundingBox.w = self.sprite:getWidth()-2
		self.boundingBox.h = self.sprite:getHeight()-2
	else 
		self.boundingBox.x = 0
		self.boundingBox.y = 0
		self.boundingBox.w = 0
		self.boundingBox.h = 0
	end
end
function cGameObject:detach()
	self.lastHolderID = self.holderID
	local go = self.holderID and CurrentLevel:objectByID(self.holderID)
	self.holderID = nil
	if (go) then
		go.attachedID = nil
	end
end