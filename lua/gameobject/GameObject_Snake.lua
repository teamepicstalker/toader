cSnake = Class{__includes={cGameObject}}
function cSnake:init(pos,properties)
	cGameObject.init(self)
	
	self.position = pos
	self.startPos = pos:clone()
	self.properties = table.copy(properties)

	self.properties.speed = (self.properties.speed or 1) * Game.difficultySpeedMult
			
	-- clamp speed
	local r = self.properties.rate > 0 and 192/self.properties.rate or self.properties.speed
	self.properties.speed = math.clamp(self.properties.speed,r,600)
	
	-- Sprite Manager
	if not (self.sprite) then
		self.sprite = cSpriteManager(self.id,pos,0,"snake_idle_0")
	else -- sprite might already exist if we are recycled
		self.sprite.id = self.id
		self.sprite.position = pos
		self.sprite:setState("snake_idle_0")
	end
	
	-- Pathing Index and Direction
	self.waypointIndex = 1
	self.waypointInc = 1
	
	-- Set Sprite according to spawn properties 
	if (self.properties.sprite) then
		local spr = self.properties.sprite[math.random(#self.properties.sprite)]
		self.sprite:setState(spr)
	end
end

function cSnake:update(dt)
	cGameObject.update(self,dt)

	if not (self.online) then
		return
	end

	
	-- outside of map, so we must tell level to recycle or destroy us and unregister as a game object
	if (self.position.x < -192 or self.position.x > mapWidth) then
		--log("hi %s",self.position.x)
		CurrentLevel:recycle(self)
		return
	end

	self.sprite:setLayer("Dynamic"..math.round(self.position.y/64))

	local way = self.properties.path[self.waypointIndex]
	if (way) then
		local x = self.startPos.x+way.x
		local y = self.startPos.y+way.y

		if (y < self.position.y) then
			self.position.y = self.position.y - 1 * self.properties.speed * dt
			self.sprite:setState("snake_idle_0")
		elseif (y > self.position.y) then
			self.position.y = self.position.y + 1 * self.properties.speed * dt
			self.sprite:setState("snake_idle_1")
		end
		if (x < self.position.x) then
			self.position.x = self.position.x - 1 * self.properties.speed * dt
			self.sprite:setState("snake_idle_0")
		elseif (x > self.position.x) then
			self.position.x = self.position.x + 1 * self.properties.speed * dt
			self.sprite:setState("snake_idle_1")
		end

		if (x+3 < self.position.x+6 and y+3 < self.position.y+6) then
			if (self.position.x+3 < x+6 and self.position.y+3 < y+6) then
				if (self.waypointIndex + self.waypointInc > #self.properties.path) then
					if (self.reverse) then
						self.waypointIndex = #self.properties.path
						self.waypointInc = -self.waypointInc
					else
						self.waypointIndex = 1
					end
				elseif (self.waypointIndex + self.waypointInc < 1) then
					self.waypointIndex = 1
					self.waypointInc = -self.waypointInc
				else
					self.waypointIndex = self.waypointIndex + self.waypointInc
				end
			end
		end
	end

	if (oPlayer.isDead ~= true and oPlayer.isMoving ~= true and not oPlayer.holderID and oPlayer:inside(self)) then
		oPlayer:makeDead()
		oPlayer.sprite:setState("dead")
		oSoundManager:playSFX("crush")
	end
end

function cSnake:resetBoundingBox()
	if (self.sprite and self.sprite.visible) then
		self.boundingBox.x = 2
		self.boundingBox.y = 2
		self.boundingBox.w = 63
		self.boundingBox.h = 63
	else
		self.boundingBox.x = 0
		self.boundingBox.y = 0
		self.boundingBox.w = 0
		self.boundingBox.h = 0
	end
end
