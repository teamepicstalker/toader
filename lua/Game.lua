cGame = Class{}
function cGame:init()
	-- setup data ini
	self.Config = cIniFile("data.ini")
	if not (self.Config) then 
		log("unable to create data.ini")
	end
	
	-- main managers
	oCallbackManager = cCallbackManager()
	oKeybindManager = cKeybindManager(self)
	oSoundManager = cSoundManager()
	oAnimationManager = cAnimationManager()
	
	-- Sending auto-loader thread callback
	loader.start(function()
		SendScriptCallback("onLoaderFinished") -- do stuff with resources before onAfterLoad
		self.finishedLoading = true
		SendScriptCallback("onAfterLoad") -- Sound and Images loaded, it's safe to use them now
	end)
	
	-- setup video settings 
	local flags = {}
	flags.vsync = self.Config:r_bool("graphics","vsync")
	flags.fullscreen = self.Config:r_bool("graphics","fullscreen")
	flags.borderless = self.Config:r_bool("graphics","borderless")
	flags.display = self.Config:r_number("graphics","display") or 1
	flags.msaa = self.Config:r_number("graphics","msaa") or 0
	flags.resizable = false
	local w = self.Config:r_number("graphics","w") or 1280
	local h = self.Config:r_number("graphics","h") or 720
	
	assert(love.window.setMode(w, h, flags),"Failed to set graphics mode. Check settings in 'AppData\\Roaming\\LOVE\\Toader\\data.ini'")
	
	-- Grab window size
    windowWidth  = love.graphics.getWidth()
    windowHeight = love.graphics.getHeight()
	
	-- Starting Values
	self.level 			= "map01"
	
	if (self.Config:r_bool("online","participate") == true) then 
		self.HighScore  = self:getNextHighScore()
	else 
		self.HighScore	= self.Config:r_number("savegame","high_score") or 0
	end
	
	self.VictoryCount	= 1
	self.ToadLives 		= 3
	self.Score 			= 0
	self.ScoreMinY 		= 0
	self.isPause 		= true
	self.updateOnPause 	= true
	self.difficultySpeedMult = 1.0
	
	DEBUG = false
	
	-- Wrap ourselves in love
	love.draw = function()
		if (self.finishedLoading) then
			self.onDraw(self)
		else 
			local percent = 0
			if loader.resourceCount ~= 0 then percent = loader.loadedCount / loader.resourceCount end
			love.graphics.print(("Loading .. %d%%"):format(percent*100), 100, 100)
		end
	end
	love.quit = function() self.onExitApp(self) end
	love.update = function(dt)
		if not (self.finishedLoading) then
			loader.update()
		else 
			self.onUpdate(self,dt) 
		end
	end
	
	RegisterScriptCallback("onAfterLoad",self.onAfterLoad,self)
end

function cGame:getNextHighScore()
	local playerid = self.Config:r_number("online","playerid")
	if (playerid) then 
		url = strformat("http://www.epicstalker.com/toader/score_script.php?gameid=1&playerid=%s&topscore=true",playerid)
	else 
		url = "http://www.epicstalker.com/toader/score_script.php?gameid=1&topscore=true"
	end 
	
	local score = self.Config:r_number("savegame","high_score") or 0
	local t = {}
	local res, code, response_headers, status = http.request{
		url = url,
		method = "GET",
		sink = ltn12.sink.table(t)
	}
	--log("%s",t[1])
	score = code == 200 and tonumber(t[1]) or score
	return score
end

function cGame:onAfterLoad()
	-- Setup Manager classes
	self.MainMenu = cMainMenu(nil,"mainMenu")
	self.MainMenu:show(true)

	-- instance a new level and re-Init
	CurrentLevel = cLevel()
	mapWidth = CurrentLevel:map().width*64
	mapHeight = CurrentLevel:map().height*64
	
	-- Setup Hud and Camera
	oHud = cHud()
	oCamera = cCamera(Vector2(640,6336))
	
	-- Player
	oPlayer = cPlayer(Vector2(640,6336))
	self.ScoreMinY = oPlayer.position.y
	oCamera:setTarget(oPlayer.id)

	-- re-Init level after player creation
	CurrentLevel:reinit()
	
	self.difficultySpeedMult = 0.7
end 

function cGame:reinit()

end

function cGame:onUpdate(dt)
	loveframes.update(dt)
	SendScriptCallback("onUpdate",dt)
	if (self.VictoryEvent) then 
		self.VictoryEvent.update(dt)
		return
	end 
	
	if (self.isPaused == true and not self.updateOnPause) then 
		return 
	end 
	
	-- Update Shared Animations
	AnimationLib["tire"].animation:update(dt)
	
	-- Update Level
	CurrentLevel:update(dt)

	if (oPlayer.isDead == true) then
		if (self.DeathEvent) then 
			self.DeathEvent.update(dt)
		else
			-- detect cheater
			if (self.ToadLives > 3+self.VictoryCount) then 
				self.ToadLives = 0
			end 
			
			self.ToadLives = self.ToadLives - 1
			if (self.ToadLives < 0) then
				self.ScoreMinY = 6336
				SendScriptCallback("onGameOver")
				self.DeathEvent = cTimer.new()
				self.DeathEvent.after(4,function()
					self.ToadLives = 3 
					self.Score = 0
					oPlayer:setPosition(640,6336)
					oPlayer:rebirth()
					self.DeathEvent = nil
				end)
			else 
				self.DeathEvent = cTimer.new()
				self.DeathEvent.after(4,function()
					oPlayer:setPosition(640,6336)
					oPlayer:rebirth()
					self.DeathEvent = nil
				end)
			end
			
			if (self.Config:r_bool("online","participate") ~= true) then
				-- save the current high score
				self.Config:SetValue("savegame","high_score",self.HighScore)
				self.Config:Save()
			elseif (Game.Score > Game.HighScore) then
				if not (Game.SubmitHighScore) then 
					Game.SubmitHighScore = cUISubmitHighScore(nil,"submitHighScore")
				end
				Game.SubmitHighScore:show(true)
			end
			
			if (Game.Score > Game.HighScore) then
				Game.HighScore = Game.Score
			end
		end
	else 
		local y = math.round(oPlayer.position.y/64)*64
		if (self.ScoreMinY > y) then
			self.Score = math.round(self.Score + ( 6336 - y ) * 0.02)
			self.ScoreMinY = y
		end
		
		if not (self.VictoryEvent) then
			if (oPlayer.grid_position.x == 576 and oPlayer.grid_position.y == 0) then
				local txt = {}
				txt[1] = { color={255,255,255,255}, font=loveframes.fonts["punkboy32"] }
				local text = {}
				
				-- Give extra life
				txt[2] = "1UP"
				text[1] = loveframes.Create("text")
				text[1]:SetShadow(true)
				text[1]:SetPos(oCamera.width-loveframes.fonts["punkboy32"]:getWidth(txt[2])/2,300,true)
				text[1]:SetText(txt)
				
				-- Inform player of game change
				txt[2] = "Game Speed increased by 5%"
				text[2] = loveframes.Create("text")
				text[2]:SetShadow(true)
				text[2]:SetPos(oCamera.width-loveframes.fonts["punkboy32"]:getWidth(txt[2])/2,400,true)
				text[2]:SetText(txt)				
				
				self.isMoving = true -- prevent moving
				self.ToadLives = self.ToadLives + 1 
				self.ScoreMinY = 6336
				self.difficultySpeedMult = self.difficultySpeedMult + 0.05
				
				self.VictoryEvent = cTimer.new()
				self.VictoryEvent.after(10,function()
					for i=1,#text do 
						text[i]:Remove()
						text[i] = nil
					end
					oPlayer:setPosition(640,6336)
					oPlayer:rebirth()
					self.VictoryEvent = nil
					self.VictoryCount = self.VictoryCount + 1
				end)
			end
		end
	end	
end 

function cGame:onDraw()
	oCamera:attach()
	CurrentLevel:draw()
	
	-- draw black outside of camera to hide sprites
	love.graphics.setColor(0, 0, 0, 255)
	love.graphics.rectangle("fill",0,oCamera.position.y-576,-256,1152)
	love.graphics.rectangle("fill",mapWidth,oCamera.position.y-576,256,1152)
	love.graphics.setColor(255, 255, 255, 255)
	
	oCamera:detach() -- Detach Camera before drawing GUI

	if (DEBUG) then
		love.graphics.print("x="..tostring(oCamera.position.x).." y="..tostring(oCamera.position.y))
		love.graphics.print("x="..tostring(oPlayer.position.x).." y="..tostring(oPlayer.position.y),0,15)
		love.graphics.print("GameObjectIDs="..tostring(CurrentLevel:GameObjectCount()),0,35)
		love.graphics.print("GameObjectIDPointer="..tostring(CurrentLevel.__gameObjectIDPointer),0,50)
	end
	-- draw UI last
	oHud:draw()
	loveframes.draw()
end 

function cGame:onExitApp()
	SendScriptCallback("onExitApp")
	if (self.Config:r_bool("online","participate") ~= true) then 
		self.Config:SetValue("savegame","high_score",self.HighScore)
	end
	self.Config:Save()
	if (gErrorLog) then 
		gErrorLog:close()
	end
end