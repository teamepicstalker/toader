local sti = require "sti"
cLevel = Class{}
function cLevel:init()

	-- Game Object Table
	self.__gameObject = {}
	self.__gameObjectCount = 0
	self.__gameObjectIDPointer = 0
	
	-- recycle/repurpose unused objects
	self.__recycle = {}
	
	-- Default draw and update methods for sprite layers 
	self.__layer_draw = function(layer)
		if (layer.sprites) then
			for _, sprite in pairs(layer.sprites) do
				if (sprite.visible) then 
					sprite:draw()
				end
			end
		end
	end
	self.__layer_update = function(layer,dt)
		if (layer.sprites) then
			for _, sprite in pairs(layer.sprites) do
				if (sprite.visible) then 
					sprite:update(dt)
				end
			end
		end
	end
	
    -- Set world meter size (in pixels)
    love.physics.setMeter(32)

    -- Load a map exported to Lua from Tiled
    self.__map = sti.new("assets/maps/"..Game.level..".lua", { "box2d" })

    -- Prepare physics world with horizontal and vertical gravity
    self.__world = love.physics.newWorld(0, 0)

    -- Prepare collision objects
    self.__map:box2d_init(self.__world)
	
	-- Create a Custom Layers
	self:setupCustomLayers()
end
function cLevel:reinit()
	-- spawn objects 
	oSpawnManager = cSpawnManager()
	
	-- create animated water tiles
	self:generateWater()
end
function cLevel:update(dt)
	oSpawnManager:update(dt)
	
	for k,v in pairs(self.__gameObject) do 
		v:update(dt)
	end

	self.__map:update(dt)
end
function cLevel:draw()
	-- Draw Range culls unnecessary tiles
    self.__map:setDrawRange(oCamera:boundary())
	
    -- Draw the map and all objects within
    self.__map:draw()

    -- Draw Collision Map (useful for debugging)
    love.graphics.setColor(255, 0, 0, 255)
    self.__map:box2d_draw()
	
	if (DEBUG) then
		for k,v in pairs(self.__gameObject) do 
			v:draw()
		end
	end

    -- Reset color
    love.graphics.setColor(255, 255, 255, 255)
end
function cLevel:addCustomLayer(layer,index)
	return self.__map:addCustomLayer(layer,index)
end
function cLevel:addSprite(layer,name,spriteTbl)
	if (self.__map.layers[layer]) then 
		if not (self.__map.layers[layer].sprites) then 
			self.__map.layers[layer].sprites = {}
		end
		--log("layer=%s name=%s",layer,name)
		self.__map.layers[layer].sprites[name] = spriteTbl
	end
end
function cLevel:world()
	return self.__world
end
function cLevel:register(obj)
	if (obj.id and not self.__gameObject[obj.id]) then 
		-- re-issue ID for a recycled object 
		self.__gameObject[obj.id] = obj
		self.__gameObjectCount = self.__gameObjectCount + 1		
		return obj.id
	end
	local id = self.__gameObjectIDPointer
	self.__gameObject[id] = obj
	self.__gameObjectCount = self.__gameObjectCount + 1
	self.__gameObjectIDPointer = self.__gameObjectIDPointer + 1
	return id
end
function cLevel:objectByID(id)
	if (self.__gameObject[id]) then 
		return self.__gameObject[id]
	end
end
function cLevel:release(obj)
	if not (obj) then
		return 
	end
	if (obj.destroy) then 
		obj:destroy()
	end
	self.__gameObject[obj.id] = nil
	self.__gameObjectCount = self.__gameObjectCount - 1
	obj = nil
end
function cLevel:recycle(obj)
	if not (obj) then
		return 
	end
	if not (obj.class) then 
		self:release(obj)
		return
	end
	
	self.__gameObject[obj.id] = nil
	self.__gameObjectCount = self.__gameObjectCount - 1
	
	if not (self.__recycle[obj.class]) then 
		self.__recycle[obj.class] = {}
	end
	
	-- Switch object offline and toggle it's switch_online flag to prevent it from coming online
	obj.switch_online = false
	obj:switchOffline()
	
	table.insert(self.__recycle[obj.class],obj)
end 
function cLevel:map()
	return self.__map
end
function cLevel:generateWater()
	local spriteTbl,pos
	for row,v in pairs(self.__map.layers[1].data) do
		for col,vv in pairs(v) do 
			if (vv.id == 0 or vv.id == 1) then
				pos = Vector2((col-1)*self.__map.tileheight,(row-1)*self.__map.tilewidth)
				spriteTbl = cSpriteManager("water"..pos.x..pos.y,pos,0,"water")
			end
		end 
	end
end
function cLevel:GameObjectCount()
	return self.__gameObjectCount
end


function cLevel:addCustomLayer(name)
	self.__map:addCustomLayer(name,#self.__map.layers+1)
	local spriteLayer = self.__map.layers[name]
	spriteLayer.draw = self.__layer_draw
	spriteLayer.update = self.__layer_update
end

function cLevel:setupCustomLayers()
	self:addCustomLayer("BelowWater")
	self:addCustomLayer("Water")
	self:addCustomLayer("AboveWater")
	
	for i=0,100 do
		self:addCustomLayer("Dynamic"..i)
	end
end

function cLevel:destroy()
	for k,v in pairs(self.__gameObject) do 
		self:release(v)
	end
end